/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import edu.ualr.oyster.core.OysterReferenceSource;
import edu.ualr.oyster.core.ReferenceItem;
import edu.ualr.oyster.formatter.ErrorFormatter;

/**
 * This class is used to parse the OysterSourceDescriptor XML file and return an 
 * instantiated <code>OysterReferenceSource</code> object.
 * @author Eric D. Nelson
 */

public class SourceDescriptorParser extends OysterXMLParser {

	/** The <code>OysterReferenceSource</code> to be populated */
    private final OysterReferenceSource sourceDescriptor;

	/** The items used during parsing */
    private final List<ReferenceItem> items;
    
    /** The <code>ReferenceItem</code> used during parsing */
    private ReferenceItem referenceItem;
    
    /** Used to hold the current XML parent tag */
    private String parent;
    
    /**
     * Creates a new instance of <code>SourceDescriptorParser</code>.
     */
    public SourceDescriptorParser () {
        this.sourceDescriptor = new OysterReferenceSource();
        this.items = new ArrayList<ReferenceItem>();
        this.referenceItem = null;
        this.parent = "";
    }

    /**
     * Called when the Parser starts parsing the Current XML File. Handle any
     * document specific initialization code here.
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void startDocument () throws org.xml.sax.SAXException {
    }

    /**
     * Called when the Parser Completes parsing the Current XML File. Handle any
     * document specific clean up code here.
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void endDocument () throws org.xml.sax.SAXException {
    }

    /**
     * Called when the starting of the Element is reached. For Example if we have
     * Tag called <Title> ... </Title>, then this method is called when <Title>
     * tag is Encountered while parsing the Current XML File. The attrs Parameter 
     * has the list of all Attributes declared for the Current Element in the 
     * XML File.
     * @param namespaceURI uri for this namespace
     * @param lName local xml name
     * @param qName qualified xml name
     * @param attrs list of all Attributes declared for the Current Element
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void startElement (String namespaceURI, String lName, String qName, Attributes attrs) throws org.xml.sax.SAXException {
        String eName = lName; // element name 
        if ("".equals(eName)) {
            eName = qName;
        } 
        
        // clear the data
        data = "";
        
        if (eName.equalsIgnoreCase("OysterSourceDescriptor")){
            parent = eName;
        } else if (eName.equalsIgnoreCase("ReferenceItems")) {
        } else if (eName.equalsIgnoreCase("Item")) {
            referenceItem = new ReferenceItem();
            parent = eName;
        } else if (eName.equalsIgnoreCase("Term")) {
            parent = eName;
        }
        
        // get XML attributes
        if (attrs != null) { 
            for (int i = 0; i < attrs.getLength(); i++) { 
                String aName = attrs.getLocalName(i); 
                // Attr name 
                if ("".equals(aName)) {
                    aName = attrs.getQName(i);
                } 
                
                String token = attrs.getValue(i).trim();
                
                if(aName.equalsIgnoreCase("Name")){
                    if (parent.equalsIgnoreCase("OysterSourceDescriptor")) {
                        sourceDescriptor.setSourceName(token);
                    } else if (parent.equalsIgnoreCase("Item")) {
                        referenceItem.setName(token);
                    }
                } else if(aName.equalsIgnoreCase("Type")){
                    sourceDescriptor.setSourceType(token);
                } else if(aName.equalsIgnoreCase("Char")){
                    sourceDescriptor.setDelimiter(token);
                } else if(aName.equalsIgnoreCase("Qual")){
                    if (token.equals("\"") || token.equals("'")) {
                        sourceDescriptor.setQualifer(token);
                    } else if (!token.equals("")) {
                        StringBuilder sb = new StringBuilder(1000);
                        sb.append("##Error: ").append(token).append(" is not an approved qualifer.");
                        Logger.getLogger(SourceDescriptorParser.class.getName()).
                            log(Level.SEVERE, sb.toString());
                    } else {
                        sourceDescriptor.setQualifer("");
                    } 
                } else if(aName.equalsIgnoreCase("Labels")){
                    if (token.equalsIgnoreCase("Y")) {
                        sourceDescriptor.setLabel(true);
                    } else {
                        sourceDescriptor.setLabel(false);
                    }
                } else if(aName.equalsIgnoreCase("Server")){
                    sourceDescriptor.setServer(token);
                } else if(aName.equalsIgnoreCase("Port")){
                    sourceDescriptor.setPort(token);
                } else if(aName.equalsIgnoreCase("SID")){
                    sourceDescriptor.setSid(token);
                } else if(aName.equalsIgnoreCase("UserID")){
                    sourceDescriptor.setUserID(token);
                } else if(aName.equalsIgnoreCase("Passwd")){
                    sourceDescriptor.setPasswd(token);
                } else if(aName.equalsIgnoreCase("CType")){
                    sourceDescriptor.setConnectionType(token);
                } else if(aName.equalsIgnoreCase("CParms")){
                    sourceDescriptor.setConnectionParms(token);
                } else if(aName.equalsIgnoreCase("Attribute")){
                    referenceItem.setAttribute(token);
                } else if(aName.equalsIgnoreCase("Format")){
                    referenceItem.setFormat(token);
                } else if(aName.equalsIgnoreCase("FormatType")){
                    referenceItem.setFormatType(token);
                } else if(aName.equalsIgnoreCase("Pos")){
                    referenceItem.setOrdinal(Integer.parseInt(token));
                } else if(aName.equalsIgnoreCase("Start")){
                    referenceItem.setStart(Integer.parseInt(token));
                } else if(aName.equalsIgnoreCase("End")){
                    referenceItem.setEnd(Integer.parseInt(token));
                }
            }
        }
    }

    /**
     * Called when the Ending of the current Element is reached. For example in 
     * the above explanation, this method is called when </Title> tag is reached
     * to capture the element value
     * 
     * @param namespaceURI uri for this namespace
     * @param sName
     * @param qName qualified xml name
     * @throws org.xml.sax.SAXException
     */
    @Override
    public void endElement (String namespaceURI, String sName, String qName) throws org.xml.sax.SAXException {
        String eName = sName; // element name 
        if ("".equals(eName)) {
            eName = qName;
        } 
        
        if (eName.equalsIgnoreCase("OysterSourceDescriptor")){
        } else if (eName.equalsIgnoreCase("Source")){
            sourceDescriptor.setSourcePath(data.trim());
        } else if (eName.equalsIgnoreCase("ReferenceItems")){
            sourceDescriptor.setReferenceItems(items);
        } else if (eName.equalsIgnoreCase("Item")){
            items.add(referenceItem);
            parent = "";
        } else if (eName.equalsIgnoreCase("OverRideSQL")){
            sourceDescriptor.setOverRideSQL(data.trim());
        }
    }
    
    /**
     * This method is the main entry point for the SAX Parser.
     * @param file the XML file to be parsed.
     * @return <code>OysterReferenceSource</code> containing data from the file.
     */
    public OysterReferenceSource parse(String file){
        // Use the default (non-validating) parser 
        SAXParserFactory factory = SAXParserFactory.newInstance(); 
        factory.setNamespaceAware(true);
        
        try {
            // Set up output stream 
            setOut(new OutputStreamWriter(System.out, "UTF8"));
            
            // Parse the input 
            SAXParser saxParser = factory.newSAXParser();
/*            
            if (saxParser.isNamespaceAware())
                System.out.println("Namespace Aware");
            else System.out.println("NOT Namespace Aware");
*/           
            // "this" class implements the xml parser methods
            saxParser.parse( new File(file), this);
        } catch (IOException ex) {
            Logger.getLogger(SourceDescriptorParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SourceDescriptorParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        } catch (SAXException ex) {
            Logger.getLogger(SourceDescriptorParser.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        }
        
        return this.sourceDescriptor;
    }
    
    // FIXME: Need to add Parser Level and XML Level validation
    // i.e. if source is of type database attributes must be Name and Attribute only
}

