package edu.ualr.oyster.utilities;

import java.util.Scanner;
import org.apache.commons.text.StrTokenizer;
import org.apache.commons.text.StrMatcher;

public class RegexTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String inString="Start";
		while (inString.length() > 0) {
			System.out.print("Enter String to Test\n");
			inString = scanner.nextLine();
            inString = inString.replaceAll("[,;:|]"," ");
   	 		System.out.println(inString);
       	 	String [] temp = inString.split(" ");
       	 	for (int j=0; j<temp.length; j++) {
       	 		String arrayString = temp[j];
       	 		if (!arrayString.isEmpty()) {
           	 		System.out.println(arrayString);
        			String firstReplace = arrayString.replaceAll("\\W+","");
        			System.out.println(firstReplace);
       	 		}
       	 	}
		}
		scanner.close();
	}
		
}
