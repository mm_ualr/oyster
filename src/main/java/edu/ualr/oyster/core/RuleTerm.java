/*
 * Copyright 2012 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.core;

import java.util.Set;
import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.functions.Compare;
import java.util.ArrayList;

/**
 * RuleTerm.java
 * Created on Mar 29, 2012 7:37:16 PM
 * @author Eric D. Nelson
 */
public class RuleTerm {
    private String item =  null;
    private Set<String> compareTo = null;
    private ArrayList<String> dataPrepSignatures = new ArrayList<String>();
    private ArrayList<Transform> dataPrepFunctions = new ArrayList<Transform>();
    private ArrayList<String> similaritySignatures = new ArrayList<String>();
    private ArrayList<Compare> similarityFunctions = new ArrayList<Compare>();
    
    /**
     * Creates a new instance of RuleTerm
     */
    public RuleTerm(){
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Set<String> getCompareTo() {
        return compareTo;
    }

    public void setCompareTo(Set<String> compareTo) {
        this.compareTo = compareTo;
    }
    
    public String getDataPrepSignature(int index) {
    	return dataPrepSignatures.get(index);
    }
    
    public void addDataPrepSignature(String function) {
    	this.dataPrepSignatures.add(function);
    } 
    
    public Transform getDataPrepFunction(int index) {
    	return dataPrepFunctions.get(index);
    }
    
    public void addDataPrepFunction(Transform function) {
    	this.dataPrepFunctions.add(function);
    }
    
    public ArrayList<Transform> getDataPrepFunctions() {
    	return dataPrepFunctions;
    }
    
    public ArrayList<String> getDataPrepSignatures() {
    	return dataPrepSignatures;
    }
    
    public String getSimilaritySignature(int index) {
    	return similaritySignatures.get(index);
    }
    
    public void addSimilaritySignature(String function) {
    	this.similaritySignatures.add(function);
    } 
    
    public Compare getSimilarityFunction(int index) {
    	return similarityFunctions.get(index);
    }
    
    public void addSimilarityFunction(Compare function) {
    	this.similarityFunctions.add(function);
    }
    
    public ArrayList<Compare> getSimilarityFunctions() {
    	return similarityFunctions;
    }
    
    public ArrayList<String> getSimilaritySignatures() {
    	return similaritySignatures;
    }

@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RuleTerm other = (RuleTerm) obj;
        if (this.item == null || !this.item.equals(other.item)) {
            return false;
        }
        if (this.compareTo == null || !this.compareTo.equals(other.compareTo)) {
            return false;
        }
        for (int j=0; j<dataPrepSignatures.size(); j++)  {
        	if (this.dataPrepSignatures.get(j) == null || !(this.dataPrepSignatures.get(j).equals(other.dataPrepSignatures.get(j)))) {
        		return false;
        	}
        }
        for (int j=0; j<similaritySignatures.size(); j++)  {
        	if (this.similaritySignatures.get(j) == null || !(this.similaritySignatures.get(j).equals(other.similaritySignatures.get(j)))) {
        		return false;
        	}
        }  
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.item != null ? this.item.hashCode() : 0);
        hash = 17 * hash + (this.compareTo != null ? this.compareTo.hashCode() : 0);
        for (int j=0; j<dataPrepSignatures.size(); j++)  {
        	hash = 17 * (this.dataPrepSignatures.get(j) != null ? this.dataPrepSignatures.get(j).hashCode() : 0) ;
        }
        for (int j=0; j<similaritySignatures.size(); j++)  {
        	hash = 17 * (this.similaritySignatures.get(j) != null ? this.similaritySignatures.get(j).hashCode() : 0) ;
        }
        return hash;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append("[item=");
        sb.append(this.item != null ? this.item : "");
        sb.append(", secItem=");
        sb.append(this.compareTo != null ? this.compareTo : "");
        for (int j=0; j<dataPrepSignatures.size(); j++)  {      
        	sb.append(", dataprep=");
        	sb.append(dataPrepSignatures.get(j));
        }   
        for (int j=0; j<similaritySignatures.size(); j++)  {      
        	sb.append(", similarity=");
        	sb.append(similaritySignatures.get(j));
        }
        sb.append("]");
        return sb.toString();
    }
}
