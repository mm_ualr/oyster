/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.ualr.oyster.io.OysterSourceReader;

/**
 * Interface to a set of entity references as implemented by the system
 * Responsibilities:
 * <ul>
 * <li>Convert an XML description of a reference source from to an 
 * OysterReferenceSource object</li>
 * <li>Allows other objects to read the references in the source in sequential 
 * order</li>
 * </ul>
 * @author Eric D. Nelson
 */

public class OysterReferenceSource {

    /** The source reader that will read this source */
    private OysterSourceReader sourceReader = null;

    /** debug this object */
    private boolean debug = false;

    /** The name of the source */
    private String sourceName = null;

    /** The path to where the source exist if it is on a file system */
    private String sourcePath = null;
    
    /** The type of the source, i.e. delimited file */
    private String sourceType = null;
    
    /** The delimiter to use on a delimited file */
    private String delimiter = null;
    
    /** The qualifer to use if the file is delimited */
    private String qualifer = null;
    
    /** The label to use if the file is delimited */
    private boolean label = false;
    
    /** The server name or IP address to use when connecting to the database */
    private String server = null;
    
    /** The port that the database will be listening on */
    private String port = null;
    
    /** The service id/name of the database */
    private String sid = null;
    
    /** The user name needed to log into the database */
    private String userID = null;
    
    /** The password to use when logging on to the database */
    private String passwd = null;
    
    /** The database connection type */
    private String connectionType = "ODBC";
    
    /** The database connection parameters */
    private String connectionParms = null;
    
    /** The custom SQL string to run */
    private String overRideSQL = null;
    
    /** The array that will hold the input references */
    private List<ReferenceItem> referenceItems = null;

    /**
     * Creates a new instance of <code>OysterReferenceSource</code>
     */
    public OysterReferenceSource () {
        this.referenceItems = new ArrayList<ReferenceItem>();
    }

    /**
     * Returns the <code>OysterSourceReader</code> for this <code>OysterReferenceSource</code>
     * @return the <code>OysterSourceReader</code>
     */
    public OysterSourceReader getSourceReader () {
        return sourceReader;
    }

    /**
     * Sets the <code>OysterSourceReader</code> for this <code>OysterReferenceSource</code>
     * @param aSourceReader the <code>OysterSourceReader</code> to be set.
     */
    public void setSourceReader (OysterSourceReader aSourceReader) {
        this.sourceReader = aSourceReader;
    }

    /**
     * Returns whether the <code>OysterReferenceSource</code> is in debug mode.
     * @return true if the <code>OysterReferenceSource</code> is in debug mode, otherwise false.
     */
    public boolean isDebug () {
        return debug;
    }

    /**
     * Enables/disables debug mode for the <code>OysterReferenceSource</code>.
     * @param debug true to enable debug mode, false to disable it.
     */
    public void setDebug (boolean debug) {
        this.debug = debug;
    }
    
    /**
     * Returns the source name for this <code>OysterReferenceSource</code>.
     * @return the source name.
     */
    public String getSourceName () {
        return sourceName;
    }

    /**
     * Sets the source name for this <code>OysterReferenceSource</code>.
     * @param aSourceName the source name to be set.
     */
    public void setSourceName (String aSourceName) {
        this.sourceName = aSourceName;
    }

    /**
     * Returns the source path for this <code>OysterReferenceSource</code>.
     * @return the source path.
     */
    public String getSourcePath () {
        return sourcePath;
    }

    /**
     * Sets the source path for this <code>OysterReferenceSource</code>.
     * @param aSourcePath the source path to be set.
     */
    public void setSourcePath (String aSourcePath) {
        this.sourcePath = aSourcePath;
    }

    /**
     * Returns the source type for this <code>OysterReferenceSource</code>.
     * @return the source type.
     */
    public String getSourceType () {
        return sourceType;
    }

    /**
     * Sets the source type for this <code>OysterReferenceSource</code>.
     * @param aSourceType The source type to be set.
     */
    public void setSourceType (String aSourceType) {
        this.sourceType = aSourceType;
    }
    
    /**
     * Returns the delimiter for this <code>OysterReferenceSource</code>.
     * @return the delimiter.
     */
    public String getDelimiter() {
        return delimiter;
    }

    /**
     * Sets the delimiter for this <code>OysterReferenceSource</code>.
     * @param aDelimiter the delimiter to be set.
     */
    public void setDelimiter(String aDelimiter) {
        this.delimiter = aDelimiter;
    }

    /**
     * Returns the text qualifier for this <code>OysterReferenceSource</code>.
     * @return the qualifier.
     */
    public String getQualifer() {
        return qualifer;
    }

    /**
     * Sets the text qualifier for this <code>OysterReferenceSource</code>.
     * @param aQualifer the text qualifier to be set.
     */
    public void setQualifer(String aQualifer) {
        this.qualifer = aQualifer;
    }

    /**
     * Returns whether the OysterReferenceSource is in debug mode.
     * @return true if the OysterReferenceSource is in debug mode, otherwise false.
     */
    public boolean isLabel() {
        return label;
    }

    /**
     * Enables/disables debug mode for the OysterReferenceSource.
     * @param aLabel true to enable debug mode, false to disable it.
     */
    public void setLabel(boolean aLabel) {
        this.label = aLabel;
    }

    /**
     * Returns the server address for this <code>OysterReferenceSource</code>.
     * @return the server address.
     */
    public String getServer() {
        return server;
    }

    /**
     * Sets the server address for this <code>OysterReferenceSource</code>.
     * @param aServer the server address to be set.
     */
    public void setServer(String aServer) {
        this.server = aServer;
    }

    /**
     * Returns the port number for the server for this <code>OysterReferenceSource</code>.
     * @return the port number.
     */
    public String getPort() {
        return port;
    }

    /**
     * Sets the port number for the server for this <code>OysterReferenceSource</code>.
     * @param aPort the port number to be set.
     */
    public void setPort(String aPort) {
        this.port = aPort;
    }

    /**
     * Returns the Database name (SID) for this <code>OysterReferenceSource</code>.
     * @return the sid
     */
    public String getSid() {
        return sid;
    }

    /**
     * Sets the Database name (SID) for this <code>OysterReferenceSource</code>.
     * @param aSid the database name to be set.
     */
    public void setSid(String aSid) {
        this.sid = aSid;
    }

    /**
     * Returns the UserID for the <code>OysterReferenceSource</code>.
     * @return the userID.
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the UserID for the <code>OysterReferenceSource</code>.
     * @param aUserID the UserID to be set.
     */
    public void setUserID(String aUserID) {
        this.userID = aUserID;
    }

    /**
     * Returns the password for this <code>OysterReferenceSource</code>.
     * @return the password.
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * Sets the password for this <code>OysterReferenceSource</code>.
     * @param aPasswd the password to be set.
     */
    public void setPasswd(String aPasswd) {
        this.passwd = aPasswd;
    }
    
    /**
     * Returns the connection type for this <code>OysterReferenceSource</code>.
     * @return the connection type to be returned.
     */
    public String getConnectionType() {
        return connectionType;
    }

    /**
     * Sets the connection type for this <code>OysterReferenceSource</code>.
     * @param connectionType the connection type to be set.
     */
    public void setConnectionType(String connectionType) {
        this.connectionType = connectionType;
    }
    
    /**
     * Returns the connection type for this <code>OysterReferenceSource</code>.
     * @return the connection type to be returned.
     */
    public String getConnectionParms() {
        return connectionParms;
    }

    /**
     * Sets the connection type for this <code>OysterReferenceSource</code>.
     * @param connectionParms the connection type to be set.
     */
    public void setConnectionParms(String connectionParms) {
        this.connectionParms = connectionParms;
    }
    
    /**
     * Returns the overRideSQL for this <code>OysterDatabaseReader</code>.
     * @return the overRideSQL.
     */
    public String getOverRideSQL() {
        return overRideSQL;
    }

    /**
     * Sets the overRideSQL for this <code>OysterDatabaseReader</code>.
     * @param overrideSQL the overRideSQL to be set.
     */
    public void setOverRideSQL(String overrideSQL) {
        overRideSQL = overrideSQL;
    }
    
    /**
     * Returns the reference items for this <code>OysterReferenceSource</code>.
     * @return the reference items.
     */
    public List<ReferenceItem> getReferenceItems () {
        return referenceItems;
    }

    /**
     * Returns a <code>ReferenceItem</code> that matches the input itemName
     * @param itemName
     * @return matched <code>ReferenceItem</code>.
     */
    public ReferenceItem getReferenceItemByItemName(String itemName){
        ReferenceItem ri = null;
        
        for (Iterator<ReferenceItem> it = referenceItems.iterator(); it.hasNext();){
            ri = it.next();
            
            if (itemName.equalsIgnoreCase(ri.getName())) {
                break;
            }
        }
        return ri;
    }
    
    /**
     * Sets the reference items for this <code>OysterReferenceSource</code>.
     * @param aReferenceItems the reference Items to be set.
     */
    public void setReferenceItems (List<ReferenceItem> aReferenceItems) {
        this.referenceItems = aReferenceItems;
    }

    /**
     * Adds the specified reference item to the end of this ArrayList, 
     * increasing its size by one.
     * @param item the item to be added.
     */
    public void addReferenceItem(ReferenceItem item){
        referenceItems.add(item);
    }

    /**
     * This method returns the value found at position index.
     * @param index
     * @return null
     */
    public String getValueAtPosition(int index) {
    	String value;
    	try {
	    	ReferenceItem item = referenceItems.get(index);
    		value = item.getData();
    	} catch (Exception ex) {
    		value = null;
    	}
    	return value;
    }

    /**
     * 
     * @param index
     * @return null
     */
    public String getAttributeOfPosition(int index) {
    	String attribute;
    	try {
	    	ReferenceItem item = referenceItems.get(index);
    		attribute = item.getAttribute();
    	} catch (Exception ex) {
    		attribute = null;
    	}
    	return attribute;
    }

//    /**
//     * 
//     * @param item
//     * @return null
//     */
//    public String getPositionOfltem (String item) {
//        return null;
//    }
//
//    /**
//     * 
//     * @param item
//     * @return null
//     */
//    public String getAttrOfltem (String item) {
//        return null;
//    }
}

