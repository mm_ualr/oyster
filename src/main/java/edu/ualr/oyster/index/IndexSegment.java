/*
 * Copyright 2012 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.index;

import edu.ualr.oyster.functions.Transform;
import edu.ualr.oyster.functions.Tokenize;

/**
 * This class is a data structure for an Index Segment
 * @author John R. Talburt, Juley 14, 2019
 */

public class IndexSegment {

    /** The attribute to be hashed */
    private String itemName;
    /** The full signature of the hash function */
    private String hashSignature;
    /** The reference to hash function object */
    private Tokenize hashFunction;
    /** The full signature of a DataPrep function */
    private String dataPrepSignature;
    /** The reference to DataPrep function object */
    private Transform dataPrepFunction;
    
    /**
     * Creates a new instance of <code>IndexRule</code>.
     */
    public IndexSegment () {
    }

    /**
     * Getters and Setters
     */
    public String getItemName() {
        return itemName;
    }
    public void setItemName(String name) {
    	itemName = name;
    }
    public String getHashSignature() {
        return hashSignature;
    }
    public void setHashSignature(String sig) {
    	hashSignature = sig;
    }
    public Tokenize getHashFunction() {
        return hashFunction;
    }
    public void setHashFunction(Tokenize func) {
    	hashFunction = func;
    }
    public String getDataPrepSignature() {
        return dataPrepSignature;
    }
    public void setDataPrepSignature(String sig) {
    	dataPrepSignature = sig;
    }
    public Transform getDataPrepFunction() {
        return dataPrepFunction;
    }
    public void setDataPrepFunction(Transform func) {
    	dataPrepFunction = func;
    }

}

