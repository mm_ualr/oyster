/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import edu.ualr.oyster.functions.Compare;

/** 
 * Proper substring transforma and compare function
 * If A is a substring of, but not equal to, B, then A is called a proper 
 * substring of B, written A ⊊ B (A is a proper substring of B) or B ⊋ A 
 * (B is a proper super-string of A).
 * http://en.wikipedia.org/wiki/Substring
 * @author Eric D. Nelson
 * 
 * The transform for this would just return the supplied string
 * This is a facade over Substring and could be deprecated
 *
 * @author James True
 */
public class SubstringProper extends Substring implements Compare {
	
	/**
	 * Constructor invokes Substring constructor with Mode indicator
	 */
	public SubstringProper() {
		super(Mode.PROPER);
	}

	@Override
	public void configure(String parameter) {
		
		// Parse the numeric minimum length
		try {
		setLength(Integer.parseUnsignedInt(parameter));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException(this.getName() + " Minimum length: " + parameter + " invalid, must be unsigned integer");
		}
	}
}
