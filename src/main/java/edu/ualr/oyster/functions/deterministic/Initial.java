package edu.ualr.oyster.functions.deterministic;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

public class Initial extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {

	/**
	 * Transforms a string into the first initial
	 * 
	 * @param arg string to transform
	 * @return first character of string or "" if arg is null or empty
	 */
	@Override
	public String transform(String arg) {
		if (!isArgValid(arg)) {
			return DEFAULT_TRANSFORM_RESULT;
		}
		return arg.substring(0, 1);
	}

	/**
	 * If one argument is an initial (1 character) and the other is not
	 * then compare the first characters of the two (case insensitive)
	 * 
	 * @param arg1 left string to compare
	 * @param arg2 right string to compare
	 * @return one argument is the initial of the other
	 */
	@Override
	public boolean compare(String arg1, String arg2) {
		if (!isArgValid(arg1, arg2)) {
			return rightAnswer(false);
		}
		// If one argument is an initial (1 char)
		if (((arg1.length() == 1) && (arg2.length() > 1)) ||
				((arg1.length() > 1) && (arg2.length() == 1)))
		{
			// If the first characters match, it is a match
			if(arg1.toUpperCase().charAt(0) == arg2.toUpperCase().charAt(0)) {
				return rightAnswer(true);
			}
		}
		return rightAnswer(false);
	}
	

}
