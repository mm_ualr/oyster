/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/*/**
 * 1. Get the first letter
 * 2. Remove Vowels, W & Y
 * 3. Encode the remaining characters
 *    b, f, p, v → 1 
 *    c, g, j, k, q, s, x, z → 2 
 *    d, t → 3 
 *    l → 4 
 *    m, n → 5 
 *    r → 6 
 * 4. Remove duplicate letters
 * 5. Create 4 digit code
 * 
 * @see <a href="http://en.wikipedia.org/wiki/Soundex"> Soundex </a>
 * @see <a href="http://www.archives.gov/genealogy/census/codec.html"> The Soundex Indexing System </a>
 * @see <a href="http://www.archives.gov/publications/general-info-leaflets/55.html"> The Soundex Indexing System </a> 
 * 
 * Soundex computes Soundex hash for the name
 * Soundex implements the compare and transform methods
 */
public class Soundex extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {

	private final org.apache.commons.codec.language.Soundex codec;

	public Soundex() {
		this.codec = new org.apache.commons.codec.language.Soundex();
	}

	public String transform(String str) {
		if (StringUtils.isBlank(str))
			return DEFAULT_TRANSFORM_RESULT;
		return codec.encode(str);
	}
}
