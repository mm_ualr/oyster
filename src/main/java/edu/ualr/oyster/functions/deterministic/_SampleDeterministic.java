/*
 * Copyright 2013-2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * Sample Deterministic Function
 * Copy this to create a new deterministic function
 * 
 * @author jatrue
 *
 */
public class _SampleDeterministic extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {
	
	/**
     * Creates a new instance of this function
     * If there is no additional initialization do to in the constructor
     * then you don't need this and can remove it
     */
    public _SampleDeterministic() {
    	// Always call the super-class constructor
		super();
		/*
		 * Do other initialization, but not configuration, work here . . .
		 */
    }

	/**
	 * Process the configuration string for this class/instance
	 * If this function does not require parameters then you don't need this
	 * method and can use the superclass method 
	 * 
	 * In this example the function takes two parameters: x and y
	 */
	@Override
	protected void configure(String parameters) {

		/*
		 * validate arguments is not null or blank
		 */
		if (!isArgValid(parameters)) {
			return;
		}

		/*
		 * Use the argument parser in the base OysterFunction class
		 */
		String[] args = parseArgs(parameters);
		
		/*
		 * Validate and store the configuration parameters.
		 * 
		 */
		if (args.length != 2) {
			throw new IllegalArgumentException(
					this.getName() + " requires two parameters: x and y");
		}
	}

	/**
	 * Default compare function, compares the results of transform on both strings
	 * If either argument is null or empty or all blanks, returns false
	 * Performs a case-sensitive comparison between the two arguments
	 *
	 * @see edu.ualr.oyster.functions.Compare#compare(java.lang.String,java.lang.String)
	 * 
	 * If you don't need any additional logic then don't code this method
	 * 
	 * @param arg1 left compare argument
	 * @param arg2 right compare argument
	 * @return true=both arguments are identical after the transform
	 * 		false=arguments are different, null, or blank
	 */
	public boolean compare(String arg1, String arg2) {
		// NPE protection
		if (!isArgValid(arg1,arg2)) {
			return rightAnswer(false);
		}
        return rightAnswer(transform(arg1).equals(transform(arg2)));
	}
	
	/**
	 * Default tokenize function
	 * Stores the result of the transform in a single element array
	 * This is used primarily for indexing which requires an array of values
	 *
	 * @see edu.ualr.oyster.functions.Tokenize#tokenize(java.lang.String)
	 * 
	 * If you don't need any additional logic then don't code this method
	 * 
	 * @param arg is the argument string to transform and return
	 * @return single element array of transformed argument, or an empty array
	 */
	public String[] tokenize(String arg) {
		// No argument returns an empty array
    	if (!isArgValid(arg)) {
    		return new String[0];
    	}
    	return new String[] {transform(arg)};
	}
	
	/*
	 * The transform method must be implemented in the subclass
	 * This is where the real work is done for compare and tokenize
	 * 
	 * @see edu.ualr.oyster.functions.Transform#transform(java.lang.String)
	 * 
	 * In this example method the passed value is returned unaltered
     */
    public String transform(String str) {
    	if (!isArgValid(str)) {
    		return DEFAULT_TRANSFORM_RESULT;
    	}
    	String result = str;
    	return result;
    }
}
