/*
 * Copyright 2010-2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * NYSIISCode.java Created on May 15, 2010
 * 
 * The New York State Identification and Intelligence System Phonetic Code,
 * commonly known as NYSIIS, is a phonetic algorithm devised in 1970 as part of
 * the New York State Identification and Intelligence System (now a part of the
 * New York State Division of Criminal Justice Services). Algorithm taken from
 * http://en.wikipedia.org/wiki/New_York_State_Identification_and_Intelligence_System.
 * 
 * More information can be found at "Name Search Techniques" by R. L. Taft.
 * 
 * @author Eric D. Nelson
 */
public class NYSIISCode extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {

	// Strict limits encoded value to 6 characters, non-strict can be any arbitrary length
	private boolean strict;
	
	// This isn't final because it may need to be replaced in the configure method
	private org.apache.commons.codec.language.Nysiis nysiis;

	// Encoding mode
	private enum Mode {
		STRICT, LAX;

		public static Mode getByName(String name) {
			for (Mode entry : Mode.values()) {
				if (StringUtils.equalsIgnoreCase(entry.name(), name)) {
					return entry;
				}
			}
			return null;
		}
	}

	public NYSIISCode() {
		super();
		// Default is strict
		this.strict = true;
		this.nysiis = new org.apache.commons.codec.language.Nysiis(this.strict);
	}

	/**
	 * Process the configuration string for this class/instance
	 */
	@Override
	protected void configure(String parameters) {

		// Save the parameters
		this.parameters = parameters;

		// No arguments is allowed
		if (!isArgValid(parameters)) {
			return;
		}

		// Only one argument token is allowed
		String[] tokens = StringUtils.split(parameters, ", ");
		if (tokens.length > 1) {
			throw new IllegalArgumentException(
					this.getName() + " the parameter should be one of: " + getOptions(Mode.class));
		}

		// Validate and get the mode
		Mode mode = Mode.getByName(tokens[0]);
		if (mode == null) {
			throw new IllegalArgumentException(
					this.getName() + " mode: " + tokens[0] + " invalid, use: " + getOptions(Mode.class));
		}
		
		// If the requested mode is not currently selected, reallocate the codec
		switch (mode) {
		case LAX:
			if (nysiis.isStrict()) {
				strict = false;
				nysiis = new org.apache.commons.codec.language.Nysiis(strict);
			}
			break;
		case STRICT:
		default:
			if (!nysiis.isStrict()) {
				strict = true;
				nysiis = new org.apache.commons.codec.language.Nysiis(strict);
			}
			break;
		}
	}

	/**
	 * This method determines the New York State Identification and Intelligence
	 * System Phonetic code for the input String.
	 * 
	 * @param str The input string
	 * @return a String containing the NYSIIS encoding or an empty string
	 */
	public String transform(String str) {
		String result = DEFAULT_TRANSFORM_RESULT;

		// Null or blank argument
		if (!isArgValid(str)) {
			return result;
		}

		try {
			result = nysiis.encode(str);
		} catch (IllegalArgumentException ex) {
			logger.error("{} : error:()", str, ex.getMessage());
		}

		return result;
	}
}
