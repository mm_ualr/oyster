/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;

/*
 * Transpose looks for differences that are adjacent
 * 
 * Transpose implements Compare
 * 
 */
public class Transpose extends OysterFunction implements Compare {

	/**
	 * Strings are equal if they are the same or differ only by
	 * on adjacent transposition
	 * 
	 * Blank or null strings are not equal
	 * 
	 * @param str1 left string to compare
	 * @param str2 right string to compare
	 * @return true=equal, false=not equal
	 */
	public boolean compare(String str1, String str2) {

		// Invalid arguments, return mismatch
		if (!isArgValid(str1,str2)) {
			return rightAnswer(false);
		}
		
		// Arguments match exactly, return match
		if (str1.equals(str2)) {
			return rightAnswer(true);
		}

		// We know they are not equal
		
		// Holders for mismatch positions, initially with invalid offsets
		int first = -1;
		int second = -1;
		
		// Compare each character up to the length of the shortest argument
		for (int i = 0; i < Math.min(str1.length(),str2.length()); i++) {
		    if (str1.charAt(i) != str2.charAt(i)) {
		        if (first == -1) {
		            first = i;
		        } else {
		            second = i;
		        }
		    }
		}
		
		/*
		 * If there is no first mismatch then they are equal up to the minimum length
		 */
		if (first == -1) {
			return rightAnswer(true);
		}
		
		/*
		 * There must be a mismatch because we know they args are not equal
		 * If they are adjacent the difference is 1
		 * If there is no second mismatch the difference will be a larger negative number
		 */
		if ((second - first) != 1) {
			return rightAnswer(false);
		}
		
		/*
		 * The mismatch is adjacent, now are the characters the same
		 */
        if (str1.charAt(first) == str2.charAt(second) &&
            str1.charAt(second) == str2.charAt(first)) {
            return rightAnswer(true);
        }

	    return rightAnswer(false);
	}
}
