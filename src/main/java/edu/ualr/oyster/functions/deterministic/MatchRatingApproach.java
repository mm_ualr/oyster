/*
 * Copyright 2010 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.deterministic;

import org.apache.commons.codec.language.MatchRatingApproachEncoder;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionDeterministic;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

/**
 * MatchRatingApproach.java
 * Created on May 16, 2010
 * 
 * This class is based on the algorithm developed by Western Airlines. More
 * information can be found at:
 * <br>
 * <ul>
 * <li>http://en.wikipedia.org/wiki/Match_Rating_Approach</li>
 * <li>"An Overview of the Issues Related to the use of Personal Identifiers" by 
 * Mark Armstrong</li>
 * </ul>  
 * <br>
 * Encoding Rules
 * 1. Delete all vowels unless the vowel begins the word 
 * 2. Remove the second consonant of any double consonants present 
 * 3. Reduce codex to 6 letters by joining the first 3 and last 3 letters only 
 * <br>
 * Comparison Rules
 * 1. If the length difference between the encoded strings is 3 or greater, then
 *    no similarity comparison is done. 
 * 2. Obtain the minimum rating value by calculating the length sum of the 
 *    encoded strings and using table A 
 * 3. Process the encoded strings from left to right and remove any identical 
 *    characters found from both strings respectively. 
 * 4. Process the unmatched characters from right to left and remove any 
 *    identical characters found from both names respectively. 
 * 5. Subtract the number of unmatched characters from 6 in the longer string. 
 *    This is the similarity rating. 
 * 6. If the similarity rating equal to or greater than the minimum rating then 
 *    the match is considered good.
 * 
 * @author Eric D. Nelson
 */
public class MatchRatingApproach extends OysterFunctionDeterministic implements Compare, Transform, Tokenize {
	
	// This isn't final because it may need to be replaced in the configure method
	private final org.apache.commons.codec.language.MatchRatingApproachEncoder codec;

    /**
     * Creates a new instance of MatchRatingApproach
     */
    public MatchRatingApproach() {
    	super();
		this.codec = new MatchRatingApproachEncoder();
    }
    
    /**
     * This method compares two String using the Western Airlines Match Rating
     * encoding. If the Strings produce the same encoding then the Strings are
     * considered a match and true is returned.
     * @param str1 input source String
     * @param str2 input target String
     * @return true if the source and target are considered a match, otherwise
     * false.
     */
    public boolean compare(String str1, String str2){
		// Ensure there is something to compare
		if (!isArgValid(str1, str2)) {
			return rightAnswer(false);
		}
		return rightAnswer(codec.isEncodeEquals(str1, str2));
    }
    
    /**
     * This method determines the Western Airlines encoding for the input String. 
     * @param str The input string
     * @return a String containing the encoding
     */
    public String transform(String str){
		// Ensure there is something to compare
		if (!isArgValid(str)) {
			return  DEFAULT_TRANSFORM_RESULT;
		}
		return codec.encode(str);
    }
}
