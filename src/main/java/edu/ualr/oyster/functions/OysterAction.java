package edu.ualr.oyster.functions;

import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public interface OysterAction {
	
	default String getAction() {
		return this.getClass().getTypeName();
	}

	/**
	 * Get the enumerated type of the function
	 * @return FunctionType of this function
	 */
	public FunctionType getType();
	
	/**
	 * Is the interface (action) implmenent by this class
	 * @param name of interface
	 * @return true=implemented false=not implemented
	 */
	public boolean isImplemented(String name);
	
	/**
	 * Is the interface (action) implmenent by this class
	 * @param class of interface
	 * @return true=implemented false=not implemented
	 */
	public boolean isImplemented(Class<? extends OysterAction> action);
	
	/**
	 * Get the normalized name of the function
	 * @return String name in upper case
	 */
	public String getName();
	
	/**
	 * Get the parmeters used to initialize the function
	 * @return Parameter string between the () delimiters in the function signature
	 */
	public String getParameters();
	
	/**
	 * Is the function to reverse (negate) its response
	 * @return true=reversed, false=normal
	 */
	public boolean isReversed();
	
	/**
	 * Normalize the boolean response depending on the signature !FUNCTION or ~FUNCTION
	 * @param input computed response
	 * @return adjusted response
	 */
	public boolean rightAnswer(boolean input);
	
	
	/**
	 * Standard parameter validation
	 * This can be used inside the function to validate the passed arguments as well as by 
	 * the function's clients to validate the returned result before further processing.
	 * 
	 * Invalid arguments are:
	 * 	null reference
	 *  empty or all spaces (blank)
	 * 
	 * @param args 1-n arguments to be validated
	 * @return true=all args are valid, false=any arg is not valid
	 */
	public boolean isArgValid(String... args);

}
