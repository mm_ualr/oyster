/*
 * Copyright 2010 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import java.util.Locale;

import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * 
 * Threshold is >= 0.25 but it can be adjusted by the user.
 * 
 * For more information see http://www.gregholland.com/greg/academics.asp ALAR
 * Preceeding 2010
 * 
 * @author Eric D. Nelson
 */
public class QGramTetrahedralRatio extends OysterFunctionProbabilistic implements Score {
	
	/**
	 * 
	 * @param str1 String one
	 * @param str2 String two
	 * @return result
	 */
	@Override
	public float score(String str1, String str2) {
		float result = -1f;

		// NPE protection
		if (!isArgValid(str1,str2)) {
			return result;
		}

		int i = 0;
		int j;
		float Q = 0;
		boolean quit = false;
		while (i <= str1.length()) {
			j = i;
			while (j < str1.length() + 1) {
				if ((str2.toLowerCase(Locale.US).indexOf(str1.toLowerCase(Locale.US).substring(i, j))) > -1
						&& !str1.toLowerCase(Locale.US).substring(i, j).equals("")) {
					Q += j - i;
				}
				j++;
			}
			i++;
		}

		if (!quit) {
			float n1 = str1.length();
			float n2 = str2.length();
			float Tn1 = (n1) * (n1 + 1) * (n1 + 2) / 6f;
			float Tn2 = (n2) * (n2 + 1) * (n2 + 2) / 6f;
			result = (n1 * Q / Tn1 + n2 * Q / Tn2) / (n1 + n2);
		}
		return result;
	}
}
