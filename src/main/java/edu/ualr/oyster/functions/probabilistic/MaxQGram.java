/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import org.apache.commons.text.similarity.LongestCommonSubsequence;
import org.apache.commons.text.similarity.LongestCommonSubsequenceDistance;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * An edit distance algorithm based on the length of the longest common subsequence between two strings.
 *
 * <p>
 * This code is directly based upon the implementation in {@link LongestCommonSubsequence}.
 * </p>
 *
 * <p>
 * For reference see: <a href="https://en.wikipedia.org/wiki/Longest_common_subsequence_problem">
 * https://en.wikipedia.org/wiki/Longest_common_subsequence_problem</a>.
 * </p>
 *
 * <p>For further reading see:</p>
 * <p>Lothaire, M. <i>Applied combinatorics on words</i>. New York: Cambridge U Press, 2005. <b>12-13</b></p>
 *
 * <p>Wraps Apache Commons LongestCommonSubsequenceDistance</p>
 * 
 * @author James True
 *
 */
public class MaxQGram extends OysterFunctionProbabilistic implements Compare, Score {

	private final LongestCommonSubsequenceDistance function;

	/*
	 * Constructor
	 * Create the LevenshteinDistance object to do the work
	 */
	public MaxQGram() {
		super();
		this.function = new LongestCommonSubsequenceDistance();
	}

	/**
	 * Computes the normalized score
	 * 
	 * @param str1 left operand
	 * @param str2 right operand
	 * @return absolute normalized score = 1.0 - distance / maximum operand length
	 */
	@Override
	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		// Because strings have been validates neither are zero length
		float maxLength = Math.max(str1.length(), str2.length());
		float score = function.apply(str1, str2);
		float result = 1.0f - (score / maxLength);
		return (result *.5f) + 0.5f;
	}

}
