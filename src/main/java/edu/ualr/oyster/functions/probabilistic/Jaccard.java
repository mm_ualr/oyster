/*
 * Copyright 2013 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import org.apache.commons.text.similarity.JaccardSimilarity;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * This class computes Jaccard distance between the characters in the two
 * strings. The Jaccard coefficient measures similarity between sample sets, and
 * is defined as the size of the intersection divided by the size of the union.
 * 
 * Created on Jul 8, 2012 1:48:40 AM
 * 
 * @author Eric D. Nelson
 */
public class Jaccard extends OysterFunctionProbabilistic implements Score, Compare {

	private final JaccardSimilarity function;

	public Jaccard() {
		super();
		this.function = new JaccardSimilarity();
	}

	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		Double result = function.apply(str1, str2);
		return result.floatValue();
	}
}
