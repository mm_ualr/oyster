/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import org.apache.commons.text.similarity.CosineDistance;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * Measures the Cosine similarity of two vectors of an inner product space and
 * compares the angle between them.
 *
 * <p>
 * For further explanation about the Cosine Similarity, refer to
 * http://en.wikipedia.org/wiki/Cosine_similarity.
 * </p>
 *
 *<p>Wraps Apache Commons CosineSimilarity</p>
 *
 * FIXME CosineDistance returns either 1.0 or 0.0 so something is wrong
 *
 * @author James True
 *
 */
public class Cosine extends OysterFunctionProbabilistic implements Compare, Score {

	private final CosineDistance function;

	/*
	 * Constructor
	 * Create the LevenshteinDistance object to do the work
	 */
	public Cosine() {
		super();
		this.function = new CosineDistance();
	}

	/**
	 * Computes the normalized score
	 * 
	 * @param str1 left operand
	 * @param str2 right operand
	 * @return normalized score
	 */
	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		// FIXME All scores seem to be either 1 or 0???
		return function.apply(str1, str2).floatValue();
	}

}
