/*
 * Copyright 2013 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * This class computes Tanimoto coefficient between the characters in the two 
 * strings. Strings that have Tanimoto coefficient values > 0.85 are generally
 * considered similar to each other.
 * 
 * Created on Jul 8, 2012 1:48:40 AM
 * @author Eric D. Nelson
 */
public class Tanimoto extends OysterFunctionProbabilistic implements Compare, Score {

    /**
     * TODO compare the result to the threashold and return true/false
     * @param str1
     * @param str2
     * @return
     */
	@Override
	public float score(String str1, String str2) {
        float score = 0;
		
        Set<Character> set = new LinkedHashSet<Character>();
        String temp1 = "";
        String temp2 = "";

        // Add characters in str1 to the set
        if (isArgValid(str1)) {
            temp1 = str1.toUpperCase(Locale.US);
            for (int i = 0; i < temp1.length(); i++) {
                set.add(temp1.charAt(i));
            }
        }
        
        // Add characters in str2 to the set
        if (isArgValid(str2)) {
            temp2 = str2.toUpperCase(Locale.US);
            for (int i = 0; i < temp2.length(); i++) {
                set.add(temp2.charAt(i));
            }
        }
        
        int c1 = 0;
        int c2 = 0;
        int c3 = 0;
        
        for (Character c : set) {
            // Count the number of occurrences of each character in str1
            if (temp1.contains(String.valueOf(c))) {
                c1++;
            }
            // Count the number of occurrences of each character in str1
            if (temp2.contains(String.valueOf(c))) {
                c2++;
            }
            // Count the number of occurrences of each character in both strings
            if (temp1.contains(String.valueOf(c)) && temp2.contains(String.valueOf(c))) {
                c3++;
            }
        }
        
        // Computer the distance
        if (c3 > 0) {
            score = (float)c3/(float)(c1+c2-c3);
        }
        return score;
    }
}
