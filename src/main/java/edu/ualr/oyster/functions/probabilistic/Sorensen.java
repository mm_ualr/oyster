/*
 * Copyright 2013 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * The Sørensen Similarity (Sørensen index, or Sørensen Similarity Coefficient).
 * The equation comes from Thorvald Sørensen, a turn-of-the-century Danish
 * botanist (Thorvald Sørensen 2011). Equation: (2 * intersect(A, B)) / (|A| +
 * |B|)
 *
 * Created on Jul 8, 2012 1:48:40 AM
 * @author Eric D. Nelson
 */
public class Sorensen extends OysterFunctionProbabilistic implements Compare, Score {

	/**
     * TODO compare the result against the threshold and return true/false
     * @param str1
     * @param str2
     * @return
     */
	@Override
    public float score(String str1, String str2) {
        float score;
        float sLen = 0;
        float tLen = 0;
        Set<Character> sSet = new LinkedHashSet<Character>();
        Set<Character> tSet = new LinkedHashSet<Character>();

        if (isArgValid(str2)) {
            String tTemp = str2.toUpperCase(Locale.US);
            for (int i = 0; i < tTemp.length(); i++) {
                tSet.add(tTemp.charAt(i));
            }
            tLen = str2.length();
        } 
        
        if (isArgValid(str1)) {
            String sTemp = str1.toUpperCase(Locale.US);
            for (int i = 0; i < sTemp.length(); i++) {
                sSet.add(sTemp.charAt(i));
            }
            sLen = str1.length();
        } 
        
        Set<Character> intersection = getIntersection(sSet, tSet);
        score = (float) (intersection.size()) / (sLen + tLen);
        return score;
    }

    /**
     * Calculate the intersection size between two character sets
     * 
     * @param sSet
     * @param tSet
     * @return
     */
    private Set<Character> getIntersection(Set<Character> sSet, Set<Character> tSet) {
        Set<Character> set = new LinkedHashSet<Character>();
        if (sSet.size() > tSet.size()) {
            set.addAll(sSet);
            set.retainAll(tSet);
        } else {
            set.addAll(tSet);
            set.retainAll(sSet);
        }
        return set;
    }
}
