/*
 * Copyright 2013 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.Score;

/**
 * This class computes Tversky Index between the characters in the two strings.
 * This is an asymmetric similarity measure. The Tversky index can be seen as a
 * generalization of Dice's coefficient and Tanimoto coefficient. Further,
 * alpha, beta >= 0 are parameters of the Tversky index. Setting alpha = beta =
 * 1 produces the Tanimoto coefficient; setting alpha = beta = 0.5 produces
 * Dice's coefficient.
 * 
 * TODO Add compare against the threashold and return true/false
 * 
 * Created on Jul 8, 2012 1:48:40 AM
 * 
 * @author Eric D. Nelson
 */
public class TverskyIndex extends OysterFunction implements Score {

	private float alpha;;

	public float getAlpha() {
		return this.alpha;
	}

	/**
	 * @param alpha the alpha to set
	 */
	public void setAlpha(String alpha) {
		try {
			setAlpha(Float.parseFloat(alpha));
		} catch (Exception e) {
			throw new IllegalArgumentException(this.getName() + " alpha is not a valid decimal value");
		}
	}
	public void setAlpha(float alpha) {
		if ((alpha < 0.0) || (alpha > 1.0)) {
			throw new IllegalArgumentException(this.getName() + " alpha value must be between 0.0 and 1.0");
		}
		this.alpha = alpha;
	}

	private float beta;

	public float getBeta() {
		return this.beta;
	}

	/**
	 * @param beta the beta to set
	 */
	public void setBeta(String beta) {
		try {
			this.beta = Float.parseFloat(beta);
		} catch (Exception e) {
			throw new IllegalArgumentException(this.getName() + " beta is not a valid decimal value");
		}
	}
	public void setBeta(float beta) {
		if ((beta < 0.0) || (beta > 1.0)) {
			throw new IllegalArgumentException(this.getName() + " beta value must be between 0.0 and 1.0");
		}
	}

	/**
	 * Constructor
	 */
	public TverskyIndex() {
		super();
		this.alpha = 1.0f;
		this.beta = 1.0f;
	}

	@Override
	protected void configure(String parameters) {

		// Tokenize on comma delimiter
		String[] tokens = StringUtils.split(parameters, ", ");
		if (tokens.length != 2) {
			throw new IllegalArgumentException(this.getName() + " parameters should be (alpha,beta)");
		}
		setAlpha(tokens[0]);
		setBeta(tokens[1]);
	}

	/**
	 * Calculate the distance between two strings
	 * 
	 * @param str1
	 * @param str2
	 * @return
	 */
	public float score(String str1, String str2) {
		Set<Character> set1 = new LinkedHashSet<Character>();
		Set<Character> set2 = new LinkedHashSet<Character>();

		if (str1 == null && str2 != null) {
			String tTemp = str2.toUpperCase(Locale.US);
			for (int i = 0; i < tTemp.length(); i++) {
				set2.add(tTemp.charAt(i));
			}
		} else if (str1 != null && str2 == null) {
			String sTemp = str1.toUpperCase(Locale.US);
			for (int i = 0; i < sTemp.length(); i++) {
				set1.add(sTemp.charAt(i));
			}
		} else if (str1 != null && str2 != null) {
			String sTemp = str1.toUpperCase(Locale.US);
			String tTemp = str2.toUpperCase(Locale.US);

			for (int i = 0; i < tTemp.length(); i++) {
				set2.add(tTemp.charAt(i));
			}

			for (int i = 0; i < sTemp.length(); i++) {
				set1.add(sTemp.charAt(i));
			}
		}

		float intersection = getIntersection(set1, set2);
		float xMinusY = getSetDifference(set1, set2);
		float yMinusX = getSetDifference(set2, set1);

		return intersection / (intersection + (alpha * xMinusY) + (beta * yMinusX));
	}

	/**
	 * Calculate the size of the intersection between to character sets
	 * 
	 * @param set1
	 * @param set2
	 * @return
	 */
	private float getIntersection(Set<Character> set1, Set<Character> set2) {
		Set<Character> set = new LinkedHashSet<Character>();
		if (set1.size() > set2.size()) {
			set.addAll(set1);
			set.retainAll(set2);
		} else {
			set.addAll(set2);
			set.retainAll(set1);
		}
		return set.size();
	}

	/**
	 * Calculate the number of differences between two character sets
	 * 
	 * @param set1
	 * @param set2
	 * @return
	 */
	private float getSetDifference(Set<Character> set1, Set<Character> set2) {
		Set<Character> set = new LinkedHashSet<Character>();
		set.addAll(set1);
		set.removeAll(set2);
		return set.size();
	}
}
