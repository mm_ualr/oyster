/*
 * Copyright 2019 John Talburt, Eric Nelson, James True
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.probabilistic;

import org.apache.commons.text.similarity.JaroWinklerDistance;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunctionProbabilistic;
import edu.ualr.oyster.functions.Score;

/**
 * A similarity algorithm indicating the percentage of matched characters between two character sequences.
 *
 * <p>
 * The Jaro measure is the weighted sum of percentage of matched characters
 * from each file and transposed characters. Winkler increased this measure
 * for matching initial characters.
 * </p>
 *
 * <p>
 * This implementation is based on the Jaro Winkler similarity algorithm
 * from <a href="http://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance">
 * http://en.wikipedia.org/wiki/Jaro%E2%80%93Winkler_distance</a>.
 * </p>
 *
 * <p>
 * This code has uses the Apache Commons implementation JaroWinklerDistance
 * </p>
 * 
 * @author James True
 *
 */
public class JaroWinkler extends OysterFunctionProbabilistic implements Compare, Score {

	private final JaroWinklerDistance function;

	/*
	 * Constructor
	 * Create the LevenshteinDistance object to do the work
	 */
	public JaroWinkler() {
		super();
		this.function = new JaroWinklerDistance();
	}

	/**
	 * Computes the normalized score
	 * 
	 * @param str1 left operand
	 * @param str2 right operand
	 * @return normalized score = 1.0 - distance / maximum operand length
	 */
	public float score(String str1, String str2) {
		if (!isArgValid(str1, str2)) {
			return 0.0f;
		}
		// Because strings have been validates neither are zero length
		return function.apply(str1, str2).floatValue();
	}

}
