package edu.ualr.oyster.functions.experimental;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ualr.oyster.formatter.ErrorFormatter;

/**
 * 
 * @author Eric D. Nelson
 */

// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.C4D7B66D-D500-28C1-7A09-6DF8995F661F]
// </editor-fold> 
public class OysterADENickNameTable {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.38B435FA-4A07-AC7B-A35B-741DA17592BB]
    // </editor-fold> 
    private Map<String, String> nicknameTable;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.189A0A2E-9D75-6884-7BC8-15815A7F92E2]
    // </editor-fold> 
    private boolean debug = false;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.370BE1BF-B9ED-F91B-2930-E7C1CDEB85AD]
    // </editor-fold> 
    public OysterADENickNameTable () {
        int count = 0;
        String read;
        String [] text;
        nicknameTable = new LinkedHashMap<String, String>();
        BufferedReader infile = null;
        
        try{
            File file = new File("data/alias.dat");
            System.out.println("Loading " + file.getName());
            infile = new BufferedReader(new FileReader(file));
            while((read = infile.readLine()) != null){
                if (!read.startsWith("!!")) {
                    text = read.split("[\t]");
                    
                    // load proper name as key and alias as value
                    nicknameTable.put(text[0].toUpperCase(Locale.US), text[1].toUpperCase(Locale.US));
                    
                    // now load the alias as the key and the proper name as tha value
                    nicknameTable.put(text[1].toUpperCase(Locale.US), text[0].toUpperCase(Locale.US));
                    
                    count++;
                }
            }
            System.out.println(count + " elements loaded.\n");
        } catch(IOException ex){
            Logger.getLogger(OysterADENickNameTable.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
        } finally {
            try {
                if (infile != null) {
            infile.close();
        }
            } catch (IOException ex) {
                Logger.getLogger(OysterADENickNameTable.class.getName()).log(Level.SEVERE, ErrorFormatter.format(ex), ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.0C76356C-4678-E026-2018-88464B38F4ED]
    // </editor-fold> 
    public Map<String, String> getNicknameTable () {
        return nicknameTable;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.EF9C84C0-CB11-EA2A-BEF6-6CA8722E56C0]
    // </editor-fold> 
    public void setNicknameTable (Map<String, String> nicknameTable) {
        this.nicknameTable.putAll(nicknameTable);
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.3571C208-86E4-2164-C3EA-6AE62AA37EBB]
    // </editor-fold> 
    public boolean isDebug () {
        return debug;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.C45ED869-695A-AF0A-2EB0-1FDEED899331]
    // </editor-fold> 
    public void setDebug (boolean debug) {
        this.debug = debug;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.8F436E85-7036-7B5A-2F70-4F7DEDCBE0D7]
    // </editor-fold> 
    /**
     * One string is an alias for the other string as defined for that attribute
     * type (William - Bill)
     * @param s source String
     * @param t target String
     * @return true if this is a known nickname pair, otherwise false
     */
    public boolean isNicknamePair (String s, String t) {
        boolean flag = false;
        if (s == null && t == null) {
            flag = false;
        } else if (s != null && t == null) {
            flag = false;
        } else if (s == null && t != null) {
            flag = false;
        } else {
            if (nicknameTable != null && (nicknameTable.get(s.toUpperCase(Locale.US))) != null) {
                flag = true;
            } else if (nicknameTable != null && (nicknameTable.get(t.toUpperCase(Locale.US))) != null) {
                flag = true;
            }
        }
        return flag;
    }
}

