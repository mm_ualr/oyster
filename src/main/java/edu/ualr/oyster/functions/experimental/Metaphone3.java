/*
 * Copyright 2013 John Talburt, Eric Nelson
 *
 * This file is part of Oyster created in the ERIQ Research Center at University of Arkansas at Little Rock.
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package edu.ualr.oyster.functions.experimental;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.Transform;

/**
 * Metaphone3.java
 * 
 * 
 * Created on Apr 29, 2012 8:38:38 AM
 * @author Eric D. Nelson
 */
public class Metaphone3 extends OysterFunction implements Compare, Transform {

    public String transform(String arg) {
        // Null or empty string
        if (StringUtils.isBlank(arg)) {
        	return "";
        }

        String result = "";
        String temp = "";

        try {
            temp = arg.toUpperCase(Locale.US).trim();

            // 1. Standardize the string by removing all punctuations and spaces
            temp = temp.replaceAll("\\p{Punct}", "");
            temp = temp.replaceAll("\\p{Space}", "");

            if (StringUtils.isBlank(temp)) {
            	return "";
            }
        } catch (RuntimeException ex) {
            logger.error("arg:{}, temp:{}, result:(), error:()", arg,temp,result,ex.getMessage());
            result = "";
        }
        return result;
    }
    
    /**
     * This method compares two String using the Metaphone3. If the 
     * Strings produce the same encoding then the Strings are considered a 
     * match and true is returned.
     * @param s input source String
     * @param t input target String
     * @return true if the source and target are considered a match, otherwise
     * false.
     */
    public boolean compare(String arg1, String arg2){
    	// Transform the two arguments
        String transformed1 = transform(arg1);
        String transformed2 = transform(arg2);
        
        // Ensure there is something to compare
        if (!isArgValid(transformed1,transformed2)) {
        	return false;
        }
        transformed1 = transformed1.trim();
        transformed2 = transformed2.trim();
        return rightAnswer(transformed1.equals(transformed2));
    }
}
