/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.functions.experimental;

import java.util.ArrayList;
import java.util.Locale;

import edu.ualr.oyster.functions.probabilistic.LevenshteinEditDistance;
import edu.ualr.oyster.reference.TokenWeightTableReference;



/**
 * Matrix Weighted takes two strings, parses the strings into a list of tokens according to
 * a regular expression given by the user. After removing any excluded tokens given by the user, 
 * all pairs of tokens between the two lists are compared using using the Levenshtein edit distance 
 * function. After scoring all pairs, the highest scores for each row and
 * column are used in a weighted averaged. If the weighted average is greater than or equal to the
 * threshold parameter, the function returns true, otherwise false. Matrix takes only one
 * control parameter
 * 1) Threshold value between 0.00 and 1.00
 */
public class MatrixComparatorWeighted {
	
    float matrixScore;
    float thresholdValue;
    TokenWeightTableReference tokenWeightTable;
    LevenshteinEditDistance led = new LevenshteinEditDistance();
    
	public MatrixComparatorWeighted () {

	}
	
	public float getMatrixScore () {
		return matrixScore;		
	}
    
    private ArrayList<String> getTokens(String inStr) {
    	inStr = inStr.toUpperCase(Locale.US);
    	String [] temp = inStr.split("[\\W+]");
    	ArrayList<String> cleanList = new ArrayList<String>();   
    	if (temp.length == 0) return cleanList;    	
    	for (int k=0; k<temp.length; k++) {
    		String token = temp[k];		
    		if(token.length()>0) {
    			cleanList.add(token);
    		}
    	}
    	return cleanList;
    }
	
	public boolean computeMatrix(float th, TokenWeightTableReference wt, String s1, String s2) {
		//Set class variable values
		thresholdValue = th;
		tokenWeightTable = wt;
		//System.out.println("Threshold= "+th+" "+s1+" "+s2);
		//System.out.println(th+" "+reg+" "+ex+" "+s1+" "+s2);
		ArrayList<String> list1 = getTokens(s1);
		//System.out.println(list1);
		int len1 = list1.size();
		ArrayList<String> list2 = getTokens(s2);
		//System.out.println(list2);
		int len2 = list2.size();
		// System.out.println(len1+" "+len2);
		// in case either or both strings are empty return false
		if (len1<1 || len2<1) return false;
		// if both strings have values, generate matrix of LED values
		float[][] matrix = new float[len1][len2];
	    int trials = Math.min(len1,len2);
	    float matrixMax = 0.0f;
	    // Collect all words with exact agreement between strings
	    //words.clear();
	    for(int i = 0; i<len1; i++) {
			for(int j = 0; j < len2; j++) {
				String token_i = list1.get(i);
				String token_j = list2.get(j);
				float dist = (float) led.score(token_i,token_j);
		    	float weight_i = tokenWeightTable.getTokenWeight(token_i);
		    	float weight_j = tokenWeightTable.getTokenWeight(token_j);
		    	float weight = Math.min(weight_i, weight_j);
		    	float adjWgt = weight*dist;
				matrix[i][j] = adjWgt;
				if (matrixMax < adjWgt) matrixMax = adjWgt;
				//System.out.println(token_i+" "+weight_i+" "+token_j+" "+weight_j+" "+dist+" "+adjWgt+" "+matrixMax);
			}
	    }
	    // if greatest cell value is less than threshold, then return false
	    if (matrixMax < thresholdValue) return false;
	    float matrixTotal = 0.0f;
	    // start process the average the largest value in each row and column
	    int loopCnt = 0;
	    for (int k=0; k<trials; k++){
	    	// search for largest value in the matrix
	    	float maxValue = 0.0f;
	    	// remember cell of largest values
	    	int save_i = 0;
	    	int save_j = 0;
	    	// start search
	    	// search_loop:
	    	for (int i=0; i<len1; i++){
	    		for (int j=0; j<len2; j++){
	    			if (matrix[i][j]>maxValue){
	    				maxValue = matrix[i][j];
	    				save_i = i;
	    				save_j = j;
	    			}			
	    		}
	    	}
	    	// only continue searching matrix if the last maximum value found is positive		    	
	    	if (maxValue<=0.001) break;
	    	loopCnt++;
    		matrixTotal = matrixTotal + maxValue;
	    	//System.out.println(list1.get(save_i)+" "+weight_i+" "+list2.get(save_j)+" "+weight_j);
	    	//System.out.println("max value= "+maxValue);
	    	//System.out.println("Running Total = "+matrixTotal);
	    	// remove this row and column from further consideration
	    	for (int i=0; i<len1; i++){
	    		matrix[i][save_j] = -1.0f;
	    	}
	    	for (int j=0; j<len2; j++){
	    		matrix[save_i][j] = -1.0f;
	    	}
	    	//for (int i=0; i<len1; i++){
	    		//for (int j=0; j<len2; j++){  			
	    			//if (j==0) System.out.format("%n%.6f ", matrix[i][j]); else System.out.format("%.6f ", matrix[i][j]);	
	    		//}
	    	//}
	    	//System.out.println();
	    	//System.out.println("Total="+matrixTotal);
	    }
	    // calculate matrix average
	    if (loopCnt>0) { 
	    	matrixScore = matrixTotal/(float)(loopCnt+1); 
	    } else {
	    	matrixScore = 0.0f;
	    }
	    //System.out.println("trials= "+trials+" matrixScore= "+matrixScore);
	    // Dump agreement words for analysis
	    //if (!words.isEmpty()) {	
	    	//try {
	    		//File file = new File(".\\MatrixComparator\\Output\\words.txt");
	    		//BufferedWriter output = new BufferedWriter(new FileWriter(file, true));
			    //Iterator<String> iterator = words.iterator();
			    //while (iterator.hasNext()){
			    	//output.write(iterator.next()+"\t"+matrixScore+"\r\n");
			    //}
			   //output.close();
	    	//} catch (IOException e) {
	    		//System.out.println("Something went wrong with word output");
	    	//}
	    //}
	    //System.out.print("Score: "+matrixScore);
	    if (matrixScore >= thresholdValue) return true; else return false;
 
}


public static void main(String[] args) {
    MatrixComparatorWeighted ed = new MatrixComparatorWeighted(); 
    TokenWeightTableReference wt = TokenWeightTableReference.getInstance();
    // B859494
    String s = "JOHN  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,875-75-7727,41998,";
    // B871851
	String t = "WILLIAM  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,866-22-8052,18264,";
	    boolean result = ed.computeMatrix(0.75f, wt, s,t);
	    System.out.print(" Score: "+result);
	}
		

}

