/*
 * Copyright 2018 John Talburt, James True, Bingyi Zhong, Xinming Li, and the OYSTER Team
 *
 * This file is part of Oyster created in the University of Arkansas at Little Rock 
 * Center for Advanced Research in Entity Resolution and Information Quality (ERIQ)
 * 
 * Oyster is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Oyster is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Oyster.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package edu.ualr.oyster.functions.experimental;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Locale;

import org.apache.commons.text.StrTokenizer;

import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.deterministic.NickName;
import edu.ualr.oyster.functions.probabilistic.LevenshteinEditDistance;

/**
 * Matrix comparator takes two strings, parses the strings into a list of tokens
 * according to a regular expression given by the user. After removing any
 * excluded tokens given by the user, all pairs of tokens between the two lists
 * are compared using using the Levenshtein edit distance function. After
 * scoring all pairs, the highest scores for each row and column are averaged.
 * If the average is greater than or equal to the user specified threshold
 * value, the function returns true, otherwise false. Matrix takes three control
 * parameters 1) Threshold value between 0.00 and 1.00, 2) Regular expression
 * for tokenizing the strings 3) Optional list of excluded tokens
 */
public class MatrixComparator2 extends OysterFunction {

	private float threshold;

	float matrixScore;
	boolean tokenTableLoaded = false;
	boolean hasStopWords = false;
	LinkedHashSet<String> excludedTokens = new LinkedHashSet<String>();
	LevenshteinEditDistance led = new LevenshteinEditDistance();
	NickName nnTable = new NickName();
	// HashSet added for weight analysis
	// HashSet<String> words = new HashSet<String>();

	public MatrixComparator2() {
		led = new LevenshteinEditDistance();
	}

	@Override
	protected void configure(String parms) {
		StrTokenizer stoken = new StrTokenizer(parms);
		stoken.setDelimiterChar(',');
		stoken.setQuoteChar('\'');
		stoken.setIgnoredChar(' ');
		String[] temp = stoken.getTokenArray();
		// The first parameter must define a match threshold in the interval [0, 1]
		if (temp.length > 0) {
			try {
				this.threshold = Float.parseFloat(temp[0].trim());
			} catch (Exception e) {
				throw new IllegalArgumentException(this.getName() + " threshold is not a valid decimal value");
			}
		} else {
			throw new IllegalArgumentException(
					this.getName() + " must define a threshold value in interval [0.0, 1.0]");
		}
		// Second parameter (Optional) defines a list of stop words separated by pipe (|) character
		if (temp.length > 1) {
			parseExcludeTokens(temp[1]);
		}
	}

	private void parseExcludeTokens(String list) {
		// NPE protection
		if (!isArgValid(list)) {
			return;
		}
		String tokens = list.trim();
		String[] myArray = tokens.split("[|]");
		if (myArray.length > 0) {
			for (int k = 0; k < myArray.length; k++) {
				String token = myArray[k];
				token = token.replaceAll("\\W", "").trim();
				if (token.length() > 0)
					this.excludedTokens.add(token.toUpperCase(Locale.US));
			}
		}
		if (this.excludedTokens.size() > 0) {
			this.hasStopWords = true;
		} else {
			this.hasStopWords = false;
		}
	}

	public float getMatrixScore() {
		return matrixScore;
	}

	private boolean isExcludedToken(String item) {
		if (excludedTokens.contains(item))
			return true;
		return false;
	}

	private ArrayList<String> getTokens(String inStr) {
		inStr = inStr.toUpperCase(Locale.US);
		String[] temp = inStr.split("[\\W]");
		ArrayList<String> cleanList = new ArrayList<String>();
		if (temp.length == 0)
			return cleanList;
		for (int k = 0; k < temp.length; k++) {
			String token = temp[k];
			if (token.length() > 0) {
				if (!isExcludedToken(token))
					cleanList.add(token);
			}
		}
		return cleanList;
	}

	public boolean computeMatrix(float th, String stopwords, String s1, String s2) {
		this.threshold = th;
		parseExcludeTokens(stopwords);
		return compare(s1,s2);

	}
	
	public boolean compare (String s1, String s2) {
		ArrayList<String> list1 = getTokens(s1);
		// System.out.println(list1);
		int len1 = list1.size();
		ArrayList<String> list2 = getTokens(s2);
		// System.out.println(list2);
		int len2 = list2.size();
		// System.out.println(len1+" "+len2);
		// in case either or both strings are empty return false
		if (len1 < 1 || len2 < 1)
			return false;
		// if both strings have values, generate matrix of LED values
		float[][] matrix = new float[len1][len2];
		int trials = Math.min(len1, len2);
		float matrixMax = 0.0f;
		// Collect all words with exact agreement between strings
		// words.clear();
		for (int i = 0; i < len1; i++) {
			for (int j = 0; j < len2; j++) {
				String str1 = list1.get(i);
				String str2 = list2.get(j);
				float dist = (float) led.score(str1, str2);
				if (nnTable.compare(str1, str2) && dist < 0.95f)
					dist = 0.95f;
				matrix[i][j] = dist;
				if (matrixMax < dist)
					matrixMax = dist;
				// if (dist==1.0) words.add(list1.get(i));
			}
		}
		// if greatest distance less than threshold, then return false
		if (matrixMax < this.threshold)
			return false;
		float matrixTotal = 0.0f;
		// start process the average the largest value in each row and column
		for (int k = 0; k < trials; k++) {
			// search for largest value in the matrix
			float maxValue = 0.0f;
			// remember cell of largest values
			int savei = 0;
			int savej = 0;
			// start search
			search_loop: for (int i = 0; i < len1; i++) {
				for (int j = 0; j < len2; j++) {
					if (matrix[i][j] > maxValue) {
						maxValue = matrix[i][j];
						savei = i;
						savej = j;
						// if max value is 1.0 no need to search further
						if (maxValue == 1.0f)
							break search_loop;
					}
				}
			}
			// only continue searching matrix if the last maximum value found is positive
			if (maxValue <= 0.0)
				break;
			matrixTotal = matrixTotal + maxValue;
			// remove this row and column from further consideration
			for (int i = 0; i < len1; i++) {
				matrix[i][savej] = -1.0f;
			}
			for (int j = 0; j < len2; j++) {
				matrix[savei][j] = -1.0f;
			}
			// for (int i=0; i<len1; i++){
			// for (int j=0; j<len2; j++){
			// if (j==0) System.out.format("%n%.6f ", matrix[i][j]); else
			// System.out.format("%.6f ", matrix[i][j]);
			// }
			// }
		}
		// calculate matrix average
		matrixScore = matrixTotal / (float) trials;
		// Dump agreement words for analysis
		// if (!words.isEmpty()) {
		// try {
		// File file = new File(".\\MatrixComparator\\Output\\words.txt");
		// BufferedWriter output = new BufferedWriter(new FileWriter(file, true));
		// Iterator<String> iterator = words.iterator();
		// while (iterator.hasNext()){
		// output.write(iterator.next()+"\t"+matrixScore+"\r\n");
		// }
		// output.close();
		// } catch (IOException e) {
		// System.out.println("Something went wrong with word output");
		// }
		// }
		// System.out.print("Score: "+matrixScore);
		if (matrixScore >= this.threshold)
			return true;
		else
			return false;
	}
	private static final String stopWords = "NC|SALEM|WINSTON|DR|RD|ALLEN|ADAMS|KERNERSVILLE|27284|27106|ST|CT|27105|27103|LN|27104|27127|27107|AVE|NA|ALEXANDER|CLEMMONS|27012|CA|27101|CIR|PFAFFTOWN|27040|ANDERSON|S|E|N|LEWISVILLE|A|27023|W|FL|RIDGE|HALL|L|RURAL|M|D|PARK|WALKERTOWN|CREEK|27045|C|TX|ALLISON|27051|ALSTON|DAVID|R|MICHAEL|ELIZABETH|B|J|AMOS|JAMES|LEE|ALLRED|JOHN|TRL|FOREST|PLACE|ADKINS|WILLIAM|ALFORD|WAY|ANN|31|OLD|THOMAS|27109|WFU|APT|68|48|17|45|H|75|ABBOTT|VILLAGE|26|PL|ALBRIGHT|82|TOBACCOVILLE|ROBERT|78|MARIE|81|66|86|LYNN|97|DENISE|CHRISTOPHER|G|70|30|38|27050|ALVAREZ|83|10|14|HILL|20|58|12|41|24|CHARLES|57|BLVD|94|43|69|52|22|23|98|73|T|79|MARY|ANTHONY|60|63|ALDRIDGE|ALDERMAN|47|32|39|19|55|88|AR|AARON|62|65|GROVE|50|F|21|15|16|59|85|AGUILAR|44|33|29|93|ALLEY|ACEVEDO|GREEN|99|49|76|80|27110|77";

	public static void main(String[] args) {
		MatrixComparator2 ed = new MatrixComparator2();
		ed.configure("0.71f,"+ stopWords);
		// B859494
		String s = "JOHN  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,875-75-7727,41998,";
		// B871851
		String t = "WILLIAM  ALLISON,205,SHAMROCK TRAIL RD,LEWISVILLE,NC,27023,866-22-8052,18264,";
		boolean result = ed.computeMatrix(0.71f, stopWords, s, t);
		System.out.print("  Result: " + result);
	}

}
