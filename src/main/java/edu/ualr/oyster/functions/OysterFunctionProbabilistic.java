package edu.ualr.oyster.functions;

import org.apache.commons.lang3.StringUtils;

/**
 * Validates and stores numeric parameter values for OysterFunctions
 * 
 * @author James True
 *
 */
public abstract class OysterFunctionProbabilistic extends OysterFunction implements Compare, Score {

	/*
	 * Threshold minimum and maximum values
	 */
	private float minThreshold;
	/**
	 * @return the minValue
	 */
	protected float getMinThreshold() {
		return minThreshold;
	}
	/**
	 * @param minimum the minimum threshold value allowed
	 */
	protected void setMinThreshold(float minimum) {
		this.minThreshold = minimum;
	}

	private float maxThreshold;

	/**
	 * @return the maxValue
	 */
	protected float getMaxThreshold() {
		return maxThreshold;
	}
	/**
	 * @param maximum the maxValue to set
	 */
	protected void setMaxThreshold(float maximum) {
		this.maxThreshold = maximum;
	}

	/**
	 * Threshold value
	 */
	protected float threshold;

	/**
	 * Return the current threshold value
	 * 
	 * @return 0.0 - 1.0
	 */
	public float getThreshold() {
		return this.threshold;
	}

	/**
	 * Set the threshold from a string value This parses the string to a float and
	 * call the numberic set'er
	 * 
	 * @param threshold as a string
	 */
	public void setThreshold(String threshold) {
		try {
			setThreshold(Float.parseFloat(StringUtils.trim(threshold)));
		} catch (Exception e) {
			throw new IllegalArgumentException(String.format("%1$s Threshold value %2$s is not a valid decimal value",
					this.getName(),threshold));
		}
	}

	public void setThreshold(float threshold) {
		if ((threshold < this.minThreshold) || (threshold > this.maxThreshold)) {
			throw new IllegalArgumentException(String.format("%1$s Threshold value %2$f must be between %3$f and %4$f",
					this.getName(),threshold,this.minThreshold, this.maxThreshold));
		}
		this.threshold = threshold;
	}

	/**
	 * No-argument constructor
	 * Defaults range values
	 */
	protected OysterFunctionProbabilistic() {
		super();
		// Initialize min and max to the normalized range
		this.minThreshold = 0.0f;
		this.maxThreshold = 1.0f;
		// Initialize the threshold to less than the minimum
		this.threshold = minThreshold - 1;
	}

	/**
	 * Constructor taking alternate minimum and maximum values
	 * @param minimum smallest value allowed for threshold
	 * @param maximum largest value allowed for threshold
	 */
	protected OysterFunctionProbabilistic(float minimum, float maximum) {
		super();
		this.minThreshold = minimum;
		this.maxThreshold = maximum;
		// Initialize the threshold to less than the minimum
		this.threshold = minThreshold - 1;
	}

	/**
	 * Default configuration is a single optional "threshold" parameter specification
	 * 
	 * NOTE the threshold parameter could be required here based on whether the class
	 * implements Compare, but then that would require that the threshold be specified
	 * when the class is being constructed for the Transform interface
	 */
	@Override
	protected void configure(String parameters) {
		if (!isArgValid(parameters)) {
			return;
		}
		setThreshold(parameters);
	}

	/**
	 * Default comparator implementation
	 * This only works if the score returns a value between minThreshold and maxThreshold
	 * Calls the scoring function and if the resulting score is:
	 *  - equal to or greater than the threshold returns true
	 *  - less than the threshold returns false
	 */
	public boolean compare(String str1, String str2) {
		// Is the threshold value set
		if (threshold < minThreshold) {
			throw new IllegalArgumentException(this.getName() + " Threashold value must be specified for Compare");
		}
		// Is the score a normalized value
		float result = score(str1,str2);
		if ((result < minThreshold) || (result > maxThreshold)) {
			throw new IllegalStateException(String.format("%1$s Score %2$f is not a normalized value",this.getName(),result));
		}
		if (result >= threshold) {
			return rightAnswer(true);
		}
		return rightAnswer(false);
	}

	@Override
	public abstract float score(String str1, String str2);
}
