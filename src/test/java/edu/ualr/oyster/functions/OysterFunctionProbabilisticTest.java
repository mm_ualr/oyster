package edu.ualr.oyster.functions;

import static org.junit.Assert.*;

import org.junit.Test;

public class OysterFunctionProbabilisticTest {
	
	/**
	 * Note that because this class is not defined as a FnctionType
	 * The FUnction name in messages will be "null"
	 * 
	 * @author James True
	 *
	 */
	private class TestClass extends OysterFunctionProbabilistic {
		
		public TestClass() {
			super();
		}
		public TestClass(float min, float max) {
			super(min,max);
		}
		@Override
		public float score(String x, String y) {
			return 0;
		}
		@Override
		public boolean compare(String arg1, String arg2) {
			return false;
		}
	}

	@Test
	public void testDefaultRange() {
		TestClass threshold = new TestClass();
		threshold.setThreshold("0.5");
		assertEquals(0.5,threshold.getThreshold(),0.01);
	}

	@Test
	public void testSetGetGoodValue() {
		TestClass threshold = new TestClass(0.0f,1.0f);
		threshold.setThreshold("0.5");
		assertEquals(0.5,threshold.getThreshold(),0.01);
	}

	@Test
	public void testSetGetBiggerValue() {
		TestClass threshold = new TestClass(-5.0f,5.0f);
		threshold.setThreshold("-2.5");
		assertEquals(-2.5,threshold.getThreshold(),0.01);
		threshold.setThreshold("2.5");
		assertEquals(2.5,threshold.getThreshold(),0.01);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOutOfDefaultRange() {
		TestClass threshold = new TestClass();
		threshold.setThreshold("1.5");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOutOfSetRange() {
		TestClass threshold = new TestClass(0.0f,3.0f);
		threshold.setThreshold(3.1f);
		assertEquals(0.5,threshold.getThreshold(),0.01);
	}

	@Test
	public void testOutOfSetRangeOutput() {
		TestClass threshold = new TestClass(0.0f,3.0f);
		try {
			// Value is out of range
			threshold.setThreshold(3.16745f);
		} catch (Exception ex) {
			// Print the message
			System.out.println(ex.getMessage());
		}
		// Default threshold is minimum (0.0) -1 = -1.0
		assertEquals(-1.0f,threshold.getThreshold(),0.01);
	}

	@Test
	public void testNonNumericOutput() {
		TestClass threshold = new TestClass(0.0f,3.0f);
		try {
			// Should throw an exception when parsing the string
			threshold.setThreshold("non-numeric");
		} catch (Exception ex) {
			// Catch it and print it
			System.out.println(ex.getMessage());
		}
		// Default threshold is minimum (0.0) -1 = -1.0
		assertEquals(-1.0f,threshold.getThreshold(),0.01);
	}

}
