package edu.ualr.oyster.functions.tokenizer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Tokenize;

public class ListTokenizerTest {

	@Test
	public void testConstructor() {
		ListTokenizer func = new ListTokenizer();
		assertEquals(FunctionType.LISTTOKENIZER, func.getType());
		assertEquals(FunctionType.LISTTOKENIZER.getFunctionName(), func.getName());
	}

	@Test
	public void testTokenize() {
		ListTokenizer func = new ListTokenizer();
		func.setListDelimiters("|");
		func.setMinimumLength(2);
		func.getStopWords().setWords("XX|PVC");
		assertEquals(3, func.tokenize("John|XX|Talburt|Pvc|Pipe|A").length);
	}

	@Test
	public void testBuildAndTokenize() {
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("ListTokenizer(|,3)").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(4, func.tokenize("John|XX|Talburt|Pvc|Pipe|A").length);
		}
	}

	@Test
	public void testBuildAndTokenizeStopWords() {
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("ListTokenizer(|,2,XX|PVC)").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(3, func.tokenize("John|XX|Talburt|Pvc|Pipe|A").length);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("ListTokenizer(|)").build();
		if (func != null) {
		}
	}
}
