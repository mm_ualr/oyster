package edu.ualr.oyster.functions.tokenizer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Tokenize;

public class ListTokenizerPVTest {

	@Test
	public void testConstructor() {
		ListTokenizerPV func = new ListTokenizerPV();
		assertEquals(FunctionType.LISTTOKENIZERPV, func.getType());
		assertEquals(FunctionType.LISTTOKENIZERPV.getFunctionName(), func.getName());
	}

	@Test
	public void testTokenize() {
		ListTokenizerPV func = new ListTokenizerPV();
		func.setListDelimiters("|");
		func.setPairDelimiters(":");
		func.getStopWords().setWords("UNKNOW|UNK|NA|?");
		assertEquals(4, func.tokenize("a:12|b: 23 |c:34 |d:  UNK|e:56|f:?|UNK:NA").length);
	}

	@Test
	public void testTokenizeNoPairs() {
		ListTokenizerPV func = new ListTokenizerPV();
		func.setListDelimiters("|");
		func.setPairDelimiters(":");
		func.getStopWords().setWords("UNKNOW|UNK|NA|?");
		assertEquals(0, func.tokenize("UNKNOW:?|d:UNK|f:?|g:||UNK:NA| h||k m ").length);
	}

	@Test
	public void testBuildAndTokenize() {
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("ListTokenizerPV(|,:)").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(7, func.tokenize("a:12|b: 23 |c:34 |d:  UNK|e:56|f:?|UNK:NA").length);
		}
	}

	@Test
	public void testBuildAndTokenizeStopWords() {
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("ListTokenizerPV(|,:,'UNKNOW|UNK|NA|?')").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(4, func.tokenize("a:12|b: 23 |c:34 |d:  UNK|e:56|f:?|UNK:NA").length);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("ListTokenizerPV(|)").build();
		if (func != null) {
		}
	}
}
