package edu.ualr.oyster.functions.tokenizer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Tokenize;

public class MatrixTokenizerTest {

	private String inStr = "String Tokenizer: Test Case, J.R. Talburt (501)352-2436";

	@Test
	public void testConstructor() {
		MatrixTokenizer func = new MatrixTokenizer();
		assertEquals(FunctionType.MATRIXTOKENIZER, func.getType());
		assertEquals(FunctionType.MATRIXTOKENIZER.getFunctionName(), func.getName());
	}

	@Test
	public void testTokenize() {
		MatrixTokenizer func = new MatrixTokenizer();
		func.configure("4,CASE|String");
		assertEquals(4, func.tokenize(inStr).length);
	}

	@Test
	public void testBuildAndTokenize() {
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("!MatrixTokenizer(4,CASE|String)").build();
			// System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
				| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals(4, func.tokenize(inStr).length);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MatrixTokenizer()").build();
		if (func != null) {
		}
	}
}
