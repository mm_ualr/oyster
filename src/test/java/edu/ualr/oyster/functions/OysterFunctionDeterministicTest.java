package edu.ualr.oyster.functions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class OysterFunctionDeterministicTest {
	
	/**
	 * Note that because this class is not defined as a FnctionType
	 * The FUnction name in messages will be "null"
	 * 
	 * @author James True
	 *
	 */
	private class TestClass extends OysterFunctionDeterministic implements Transform, Tokenize, Compare{
		
		public TestClass() {
			super();
		}
		@Override
		public String transform(String arg) {
			return arg;
		}
	}

	@Test
	public void testTransform() {
		TestClass func = new TestClass();
		assertEquals("XYZ", func.transform("XYZ"));
	}

	@Test
	public void testTokenize() {
		TestClass func = new TestClass();
		assertEquals("XYZ", func.tokenize("XYZ")[0]);
	}

	@Test
	public void testCompare() {
		TestClass func = new TestClass();
		assertTrue(func.compare("XYZ","XYZ"));
	}
}
