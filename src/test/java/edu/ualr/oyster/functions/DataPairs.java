package edu.ualr.oyster.functions;

public class DataPairs {
	
	public static String[][] entries = {
		{"CRUSAN","CHRZAN"},
        {"ISLE","ISELEY"},
        {"PENNING","PENINGTON"},
        {"PENNINGTON","PENINGTON"},
        {"STROHMAN","STROHM"},
        {"EDUARDO","EDWARD"},
        {"JOHNSON","JAMISON"},
        {"RAPHAEL","RAFAEL"},
        {"SMITH","SMITHE"},
        {"TRUE","TRUELY"},
        {"TRUE","Truely"},
        {"Truely","TRUE"},
        {"SARAH","Molly"},
        {"Eric",""},
        {"",null},
        {"",""}
	};
}
