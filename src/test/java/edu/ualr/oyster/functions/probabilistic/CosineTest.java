package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.DataPairs;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.Builder;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.probabilistic.Cosine;

public class CosineTest {

	
	@Test
	public void testConstructor() {
		Cosine func = new Cosine();
		assertEquals(FunctionType.COSINE, func.getType());
		assertEquals(FunctionType.COSINE.getFunctionName(),func.getName());
	}

	/*
	 * Just get scores and print them to see how this works
	 * It uses the DataPairs class as a source of tokens
	 * Change @Test to @Ignore to turn this test off
	 */
	@Test
	public void determineScores() {
		Cosine func = new Cosine();
		for (int i = 0; i < DataPairs.entries.length; i++) {
			String str1 = DataPairs.entries[i][0];
			String str2 = DataPairs.entries[i][1];
			float score = func.score(str1,str2);
			System.out.println(str1 + " : " + str2 + " = " + score);
		}
	}
	
	@Test
	public void testCompare() {
		Cosine func = new Cosine();
		func.setThreshold(0.83f);
		assertTrue(func.compare("SMITH","SMITHE"));
		assertTrue(func.compare("TRUE","Truely"));
	}
	
	@Test
	public void testScore() {
		Cosine func = new Cosine();
		assertEquals(1.0,func.score("Sarah","Julian"),0.1);
		assertEquals(1.0,func.score("SMITH","SMITHE"),0.1);
		assertEquals(1.0,func.score("TRUE","Truely"),0.1);
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("COSINE(0.83)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("COSINE").build();
		if (func != null) {
			assertTrue(func.compare("SMITH","SMITHE"));
		}
	}
}
