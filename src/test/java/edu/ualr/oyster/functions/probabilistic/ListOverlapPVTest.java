package edu.ualr.oyster.functions.probabilistic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class ListOverlapPVTest {
	
	private String str1 = "a:12|b:23|c:34|d:UNK|e:56|f:?|UNK:NA";
	private String str2 = "a:12|UNKNOW:?|c:34|c:34|d:UNK|f:?|g:||UNK:NA|g:23| h||k|m ";
	
	@Test
	public void testConstructor() {
		ListOverlapPV func = new ListOverlapPV();
		assertEquals(FunctionType.LISTOVERLAPPV, func.getType());
		assertEquals(FunctionType.LISTOVERLAPPV.getFunctionName(),func.getName());
	}
	
	@Test
	public void testCompare() {
		ListOverlapPV func = new ListOverlapPV();
		func.setThreshold(0.50f);
		func.setListDelimiter("|");
		func.setPairDelimiter(":");
		func.setPlaceHolderValues("UNKNOW|UNK|NA|?");
		assertTrue(func.compare(str1,str2));
	}
	
	@Test
	public void testScore() {
		ListOverlapPV func = new ListOverlapPV();
		func.setThreshold(0.50f);
		func.setListDelimiter("|");
		func.setPairDelimiter(":");
		func.setPlaceHolderValues("UNKNOW|UNK|NA|?");
		assertEquals(0.60,func.score(str1,str2),0.1);
	}
	
	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("LISTOVERLAPPV(0.5,|,:,UNKNOW|UNK|NA|?)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare(str1,str2));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("LISTOVERLAPPV").build();
		if (func != null) {
			assertTrue(func.compare(str1,str2));
		}
	}

}
