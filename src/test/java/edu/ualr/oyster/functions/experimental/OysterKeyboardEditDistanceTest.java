package edu.ualr.oyster.functions.experimental;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;

import edu.ualr.oyster.functions.experimental.OysterKeyboardEditDistance;

public class OysterKeyboardEditDistanceTest {

	@Test
	public void testOysterKeyboardEditDistance() {
		OysterKeyboardEditDistance ked = new OysterKeyboardEditDistance();
		assertNotNull(ked);
	}

	@Test
	public void testGetDistance() {
	}

	@Ignore
	public void testSetDistance() {
		fail("Not yet implemented");
	}

	@Ignore
	public void testClear() {
		fail("Not yet implemented");
	}

	@Ignore
	public void testComputeDistance() {
		fail("Not yet implemented");
	}

	@Test
	public void testComputeNormalizedScore() {
        String [][] strings = {
        		{"Saturday", "Sunday"},
                {"kitten","sitting"},
                {"Robert","Rovert"}
                };
		OysterKeyboardEditDistance ked = new OysterKeyboardEditDistance();
		for (int i = 0; i < strings.length; i++) {
			int result = ked.computeDistance(strings[i][0], strings[i][1]);
			System.out.println(strings[i][0] + "\t" + strings[i][1] + "\t" + result);
		}
	}
	
	/**
	 * Read a file from the classpath which includes the resources folder
	 * 
	 * @param fileName
	 * @return Contents as string 
	 */
	private String getFile(String fileName){

		  String result = "";

		  ClassLoader classLoader = getClass().getClassLoader();
		  try {
			result = IOUtils.toString(classLoader.getResourceAsStream(fileName),StandardCharsets.UTF_8);
		  } catch (IOException e) {
			e.printStackTrace();
		  }

		  return result;

	  }

}
