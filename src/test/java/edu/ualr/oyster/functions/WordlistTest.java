package edu.ualr.oyster.functions;

import static org.junit.Assert.*;

import org.junit.Test;

public class WordlistTest {

	@Test
	public void testWordlist() {
		Wordlist list = new Wordlist();
		assertEquals('|', list.getDelimiter());
		assertFalse(list.hasWords());
		list.setDelimiter('.');
		assertEquals('.', list.getDelimiter());
		list.setWords("ONE.two.Three.fouR");
		assertEquals(4,list.getStopWords().size());
	}

	@Test
	public void testWordlistString() {
		Wordlist list = new Wordlist("ONE|two|Three|fouR");
		assertEquals('|', list.getDelimiter());
		assertTrue(list.hasWords());
		assertTrue(list.isWord("THREE"));
		assertTrue(list.isWord("one"));
		assertFalse(list.isWord("five"));
	}

	@Test
	public void testWordlistCharString() {
		Wordlist list = new Wordlist('.',"ONE.two.Three.fouR");
		assertEquals('.', list.getDelimiter());
		assertTrue(list.hasWords());
		assertTrue(list.isWord("THREE"));
		assertTrue(list.isWord("one"));
		assertFalse(list.isWord("five"));
	}
}
