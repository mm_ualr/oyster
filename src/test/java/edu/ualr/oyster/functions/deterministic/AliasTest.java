package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class AliasTest {

	@Test
	public void testConstructor() {
		Alias func = new Alias();
		assertEquals(FunctionType.ALIAS, func.getType());
		assertEquals(FunctionType.ALIAS.getFunctionName(),func.getName());
	}

	@Test(expected = java.lang.IllegalStateException.class)
	public void testConfigureBad() {
		System.out.println("testConfigureBad");
		Alias func = new Alias();
		func.configure("data/bad-alias.dat");
		System.out.println("Alias file is:" + func.getPathname());
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testConfigureNull() {
		System.out.println("testConfigureNull");
		Alias func = new Alias();
		func.configure(null);
		System.out.println("Alias file is:" + func.getPathname());
	}

	@Test
	public void testConfigure() {
		System.out.println("testConfigure");
		Alias func = new Alias();
		func.configure("data/alias.dat");
		System.out.println("Alias file is:" + func.getPathname());

		assertTrue(func.compare("ABIJAH", "AB"));
		assertFalse(func.compare("ABIJAH", "ZEBRA"));
	}

	@Test
	public void testBuildAndCompare() {
		System.out.println("testBuildAndCompare");
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Alias(data/alias.dat)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABIJAH", "AB"));
			assertFalse(func.compare("ABIJAH", "ZEBRA"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		System.out.println("testBuildAndCompareDefault");
		Compare func = new OysterFunction.Builder<Compare>("Alias").build();
		if (func != null) {
			assertTrue(func.compare("ABIJAH", "AB"));
			assertFalse(func.compare("ABIJAH", "ZEBRA"));
		}
	}

}
