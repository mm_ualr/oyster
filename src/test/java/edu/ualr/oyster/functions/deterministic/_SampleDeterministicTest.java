package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

/*
 * This is a sample unit test for a deterministic function
 * 
 * Copy this file and create a unit test for your function
 * follow the naming pattern <Function>Test
 * 
 * Edit that file and change EXACT to your function name with 
 * proper casing for the Function enumeration name (CAPS)
 * and the class name (CamelCase)
 * 
 * Then change the data and the expected result in each test
 * to match valid values for your function
 * 
 * @author James True
 */
public class _SampleDeterministicTest {

	/*
	 * Test the constructor and that the function name can be resolved
	 */
	@Test
	public void testConstructor() {
		Exact func = new Exact();
		assertEquals(FunctionType.EXACT, func.getType());
		assertEquals(FunctionType.EXACT.getFunctionName(),func.getName());
	}

	/*
	 * Test the transform function
	 * This takes attribute strings and expects property transformed (hashed) results
	 */
	@Test
	public void testTransform() {
		Exact func = new Exact();
		assertEquals("ABCDEF",func.transform("ABCDEF"));
		assertEquals("",func.transform(null));
	}

	/*
	 * Test the tokenize method
	 * It shoudl return a string array of transformed value(s)
	 */
	@Test
	public void testTokenize() {
		Exact func = new Exact();
		String [] tokens = func.tokenize("ABCDEF");
		assertEquals(1,tokens.length);
		assertEquals("ABCDEF",tokens[0]);
	}

	/*
	 * Test the compare method for both true and false results
	 */
	@Test
	public void testCompare() {
		Exact func = new Exact();
		assertTrue(func.compare("ABCDEF","ABCDEF"));
		assertFalse(func.compare("ABCDEF","abcdef"));
	}

	/*
	 * This test uses the OysterFunction Builder to construct the class
	 * This will validate that the enumeration is correct and that all
	 * the resources are accessible.
	 */
	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Exact").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCDEF","ABCDEF"));
		}
	}

	/*
	 * This test expects an IllegalArgumentException because the function
	 * does not take parameters in its signature so an error is the expected result
	 */
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Exact(XYZ)").build();
		if (func != null) {
		}
	}
}
