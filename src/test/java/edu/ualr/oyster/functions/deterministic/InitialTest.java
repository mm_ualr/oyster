package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class InitialTest {
	
	@Test
	public void testConstructor() {
		Initial func = new Initial();
		assertEquals(FunctionType.INITIAL, func.getType());
		assertEquals(FunctionType.INITIAL.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Initial func = new Initial();
		assertEquals("A",func.transform("ABCDEF"));
		assertEquals("",func.transform(""));
		assertEquals("",func.transform(null));
	}

	@Test
	public void testTokenize() {
		Initial func = new Initial();
		assertEquals("A",func.tokenize("ABCDEF")[0]);
	}

	@Test
	public void testCompare() {
		Initial func = new Initial();
		assertTrue(func.compare("A","ABCDEF"));
		assertTrue(func.compare("ABCDEF","A"));
		assertFalse(func.compare("A","A"));
		assertFalse(func.compare("","A"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("INITIAL").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("A","ABCDEF"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("INITIAL(6)").build();
		if (func != null) {
			assertTrue(func.compare("A","ABCDEF"));
		}
	}

}
