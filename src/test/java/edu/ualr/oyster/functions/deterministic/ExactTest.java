package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class ExactTest {

	@Test
	public void testConstructor() {
		Exact func = new Exact();
		assertEquals(FunctionType.EXACT, func.getType());
		assertEquals(FunctionType.EXACT.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Exact func = new Exact();
		assertEquals("ABCDEF",func.transform("ABCDEF"));
		assertEquals("",func.transform(null));
	}

	@Test
	public void testTokenize() {
		Exact func = new Exact();
		String [] tokens = func.tokenize("ABCDEF");
		assertEquals(1,tokens.length);
		assertEquals("ABCDEF",tokens[0]);
	}

	@Test
	public void testCompare() {
		Exact func = new Exact();
		assertTrue(func.compare("ABCDEF","ABCDEF"));
		assertFalse(func.compare("ABCDEF","abcdef"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Exact").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCDEF","ABCDEF"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Exact(XYZ)").build();
		if (func != null) {
		}
	}
}
