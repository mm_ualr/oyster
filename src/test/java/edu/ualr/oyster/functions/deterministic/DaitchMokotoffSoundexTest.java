package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class DaitchMokotoffSoundexTest {

	@Test
	public void testConstructor() {
		DaitchMokotoffSoundex func = new DaitchMokotoffSoundex();
		assertEquals(FunctionType.DMSOUNDEX, func.getType());
		assertEquals(FunctionType.DMSOUNDEX.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		DaitchMokotoffSoundex func = new DaitchMokotoffSoundex();
		assertEquals("074370",func.transform("abcdef"));
		assertEquals("074370",func.transform("ABCDEF"));
	}

	@Test
	public void testCompare() {
		DaitchMokotoffSoundex func = new DaitchMokotoffSoundex();
		assertTrue(func.compare("ABCDEF","abcdef"));
	}
	
	@Test
	public void testTokenize() {
		DaitchMokotoffSoundex func = new DaitchMokotoffSoundex();
		String [] expected = new String[] {"074370","075370"};
		String [] actual = func.tokenize("abcdef");
		assertEquals(expected.length, actual.length);
		for (int i = 0; i < expected.length; i++) {
			assertEquals(expected[i],actual[i]);
		}
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("DMSOUNDEX").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCDEF","abcdef"));
		}
	}

	@Test
	public void testBuildAndCompareNoFolding() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("DMSOUNDEX(NOFOLDING)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCDEF","abcdef"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("DMSOUNDEX(ABC,DEF)").build();
		if (func != null) {
		}
	}

}
