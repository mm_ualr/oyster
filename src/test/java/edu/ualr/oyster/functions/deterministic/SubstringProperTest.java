package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class SubstringProperTest {

	@Test
	public void testConstructor() {
		SubstringProper func = new SubstringProper();
		assertEquals(FunctionType.PSUBSTR, func.getType());
		assertEquals(FunctionType.PSUBSTR.getFunctionName(),func.getName());
	}

	@Test
	public void testCompare() {
		SubstringProper func = new SubstringProper();
		func.setLength(3);
		assertTrue(func.compare("ABCD123","D12"));
		assertTrue(func.compare("CD12","ABCD123"));
		assertFalse(func.compare("ABCD123","D1"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("PSubStr(3)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCD123","D12"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("PSubStr").build();
		if (func != null) {
		}
	}
}
