package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class SubstringLeftTest {

	@Test
	public void testConstructor() {
		SubstringLeft func = new SubstringLeft();
		assertEquals(FunctionType.SUBSTRLEFT, func.getType());
		assertEquals(FunctionType.SUBSTRLEFT.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		SubstringLeft func = new SubstringLeft();
		func.setLength(4);
		assertEquals("ABCD",func.transform("ABCDEF"));
		assertEquals("ABC",func.transform("ABC"));
	}

	@Test
	public void testTokenize() {
		SubstringLeft func = new SubstringLeft();
		func.setLength(4);
		assertEquals("ABCD",func.tokenize("ABCDEF")[0]);
	}

	@Test
	public void testCompare() {
		SubstringLeft func = new SubstringLeft();
		func.setLength(4);
		assertTrue(func.compare("ABCD123","ABCDEFG"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStrLeft(4)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCD123","ABCDEDFG"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("SubStrLeft").build();
		if (func != null) {
		}
	}
}
