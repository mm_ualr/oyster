package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class SubstringRightTest {

	@Test
	public void testConstructor() {
		SubstringRight func = new SubstringRight();
		assertEquals(FunctionType.SUBSTRRIGHT, func.getType());
		assertEquals(FunctionType.SUBSTRRIGHT.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		SubstringRight func = new SubstringRight();
		func.setLength(4);
		assertEquals("CDEF",func.transform("ABCDEF"));
		assertEquals("ABC",func.transform("ABC"));
	}

	@Test
	public void testTokenize() {
		SubstringRight func = new SubstringRight();
		func.setLength(4);
		assertEquals("CDEF",func.tokenize("ABCDEF")[0]);
	}

	@Test
	public void testCompare() {
		SubstringRight func = new SubstringRight();
		func.setLength(4);
		assertTrue(func.compare("123DEFG","ABCDEFG"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("SubStrRight(4)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("123DEFG","ABCDEFG"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("SubStrRight").build();
		if (func != null) {
		}
	}
}
