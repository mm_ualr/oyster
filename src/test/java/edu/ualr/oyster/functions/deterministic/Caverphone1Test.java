package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class Caverphone1Test {

	@Test
	public void testConstructor() {
		Caverphone1 func = new Caverphone1();
		assertEquals(FunctionType.CAVERPHONE, func.getType());
		assertEquals(FunctionType.CAVERPHONE.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Caverphone1 func = new Caverphone1();
		assertEquals("APKTF1",func.transform("abcdef"));
	}

	@Test
	public void testTokenize() {
		Caverphone1 func = new Caverphone1();
		assertEquals("APKTF1",func.tokenize("abcdef")[0]);
	}

	@Test
	public void testCompare() {
		Caverphone1 func = new Caverphone1();
		assertTrue(func.compare("APKTF1","abcdef"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Caverphone").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("APKTF1","abcdef"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("CAVERPHONE(ABC)").build();
		if (func != null) {
		}
	}
}
