package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class SoundexTest {

	@Test
	public void testConstructor() {
		Soundex func = new Soundex();
		assertEquals(FunctionType.SOUNDEX, func.getType());
		assertEquals(FunctionType.SOUNDEX.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Soundex func = new Soundex();
		assertEquals("A123",func.transform("ABCDEF"));
		assertEquals("A123",func.transform("ABCDEF12GH"));
		assertEquals("A120",func.transform("ABC"));
	}

	@Test
	public void testTokenize() {
		Soundex func = new Soundex();
		String [] tokens = func.tokenize("ABCDEF");
		assertEquals(1,tokens.length);
		assertEquals("A123",tokens[0]);
	}

	@Test
	public void testCompare() {
		Soundex func = new Soundex();
		assertTrue(func.compare("ABCD123","ABCDEF"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Soundex").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCD123","ABCDEDFG"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Soundex(XYZ)").build();
		if (func != null) {
		}
	}
}
