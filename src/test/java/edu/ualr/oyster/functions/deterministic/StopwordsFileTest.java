package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;
import edu.ualr.oyster.functions.Tokenize;
import edu.ualr.oyster.functions.Transform;

public class StopwordsFileTest {

	@Test
	public void testConstructor() {
		StopwordsFile func = new StopwordsFile();
		assertEquals(FunctionType.STOPWORDSFILE, func.getType());
		assertEquals(FunctionType.STOPWORDSFILE.getFunctionName(),func.getName());
	}
	@Test(expected = java.lang.IllegalStateException.class)
	public void testConfigureBad() {
		//System.out.println("testConfigureBad");
		StopwordsFile func = new StopwordsFile();
		func.configure("data/bad-stopword.dat");
		//System.out.println("StopwordsFile file is:" + func.getPathname());
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testConfigureNull() {
		//System.out.println("testConfigureNull");
		StopwordsFile func = new StopwordsFile();
		func.configure(null);
		//System.out.println("StopwordsFile file is:" + func.getPathname());
	}

	@Test
	public void testConfigure() {
		//System.out.println("testConfigure");
		StopwordsFile func = new StopwordsFile();
		func.configure("data/stop-words.dat");
		//System.out.println("StopwordsFile file is:" + func.getPathname());
	}

	@Test
	public void testBuildAndCompare() {
		//System.out.println("testBuildAndCompare");
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("stopwordsfile(data/stop-words.dat)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("JOHN", "JOHN"));
			assertFalse(func.compare("SUE", "JONN"));
			assertFalse(func.compare("THE", "THE"));
		}
	}

	@Test
	public void testBuildAndTransform() {
		//System.out.println("testBuildAndCompare");
		Transform func = null;
		try {
			func = new OysterFunction.Builder<Transform>("stopwordsFile(data/stop-words.dat)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals("JOHN",func.transform("JOHN"));
			assertEquals("",func.transform("tHe"));
		}
	}

	@Test
	public void testBuildAndTokenize() {
		//System.out.println("testBuildAndCompare");
		Tokenize func = null;
		try {
			func = new OysterFunction.Builder<Tokenize>("stopwordsFile(data/stop-words.dat)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertEquals("JOHN",func.tokenize("JOHN")[0]);
			assertEquals("",func.tokenize("tHe")[0]);
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		//System.out.println("testBuildAndCompareDefault");
		Compare func = new OysterFunction.Builder<Compare>("StopwordsFile").build();
		if (func != null) {
			assertTrue(func.compare("JOHN", "JOHN"));
			assertFalse(func.compare("SUE", "JONN"));
			assertFalse(func.compare("THE", "THE"));
		}
	}

}
