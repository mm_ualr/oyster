package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class MatchRatingApproachTest {

	@Test
	public void testConstructor() {
		MatchRatingApproach func = new MatchRatingApproach();
		assertEquals(FunctionType.MATCHRATING, func.getType());
		assertEquals(FunctionType.MATCHRATING.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		MatchRatingApproach func = new MatchRatingApproach();
		assertEquals("ABCDF",func.transform("abcdef"));
	}

	@Test
	public void testTokenize() {
		MatchRatingApproach func = new MatchRatingApproach();
		assertEquals("ABCDF",func.tokenize("abcdef")[0]);
	}

	@Test
	public void testCompare() {
		MatchRatingApproach func = new MatchRatingApproach();
		assertTrue(func.compare("ABCDIF","abcdef"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("MatchRating").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("ABCDIF","abcdef"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("MatchRATING(ABC)").build();
		if (func != null) {
		}
	}
}
