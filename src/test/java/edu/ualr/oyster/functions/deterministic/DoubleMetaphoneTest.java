package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class DoubleMetaphoneTest {

	@Test
	public void testConstructor() {
		DoubleMetaphone func = new DoubleMetaphone();
		assertEquals(FunctionType.METAPHONE2, func.getType());
		assertEquals(FunctionType.METAPHONE2.getFunctionName(),func.getName());
	}

	@Test
	public void testCompare() {
        DoubleMetaphone func = new DoubleMetaphone();
		assertTrue(func.compare("Nelson","Neilsen"));
	}

	@Test
	public void testTokenize() {
        DoubleMetaphone func = new DoubleMetaphone();
		assertEquals("NLSN",func.tokenize("Nelson")[0]);
	}
	
	/*
	 * Since all the transform tests follow the same pattern
	 * The funcionality is encapsulated in this test() method
	 * Failed tests will show this and the calling method in the
	 * stack trace
	 * @Param String term = string to be evaluated
	 * @Param String... = array of expected results, for this there shoudl be two.
	 */
	private void testTransform(String term, String expected1, String expected2) {
		DoubleMetaphone dm = new DoubleMetaphone();
		DoubleMetaphone dma = new DoubleMetaphone();
		dma.setAlternate(true);
		assertEquals(expected1,dm.transform(term));
		assertEquals(expected2,dma.transform(term));
	}

	@Test
	public void testTransform1() {
		// "Nelson" - "NLSN" and "NLSN"
		testTransform("Nelson","NLSN","NLSN");
		// "Neilson" - "NLSN" and "NLSN"
		testTransform("Neilsen","NLSN","NLSN");
	}
	
	@Test
	public void testTransform2() {
		// "Occasionally" - "AKSN" and "AKXN"
		testTransform("Occasionally","AKSN","AKXN");
	}
	
	@Test
	public void testTransform3() {
		// "antidisestablishmentarianism" - "ANTT"
		testTransform("antidisestablishmentarianism","ANTT","ANTT");
	}
	
	@Test
	public void testTransform4() {
		// "appreciated" - "APRS" and "APRX"
		testTransform("appreciated","APRS","APRX");
	}
	
	@Test
	public void testTransform6() {
		// "beginning" - "PJNN" and "PKNN"
		testTransform("beginning","PJNN","PKNN");
	}
	
	@Test
	public void testTransform7() {
		// "changing" - "XNJN" and "XNKN"
		testTransform("changing","XNJN","XNKN");
	}

	@Test
	public void testTransform9() {
		// "cheat" - "XT"
		testTransform("cheat","XT","XT");
	}

	@Test
	public void testTransform10() {
		// "dangerous" - "TNKR" and "TNJR"
		testTransform("dangerous","TNJR","TNKR");
	}

	@Test
	public void testTransform11() {
		// "development" - "TFLP"
		testTransform("development","TFLP","TFLP");
	}

	@Test
	public void testTransform12() {
		// "etiology" - "ATLJ" and "ATLK"
		testTransform("etiology","ATLJ","ATLK");
	}

	@Test
	public void testTransform13() {
		// "existence" - "AKSS"
		testTransform("existence","AKSS","AKSS");
	}

	@Test
	public void testTransform14() {
		// "simplicity" - "SMPL"
		testTransform("simplicity","SMPL","SMPL");
	}

	@Test
	public void testTransform15() {
		// "circumstances" - "SRKM"
		testTransform("circumstances","SRKM","SRKM");
	}

	@Test
	public void testTransform16() {
		// "fiery" - "FR"
		testTransform("fiery","FR","FR");
	}

	@Test
	public void testTransform17() {
		// "february" - "FPRR"
		testTransform("february","FPRR","FPRR");
	}

	@Test
	public void testTransform18() {
		// "illegitimate" - "ALJT" and "ALKT"
		testTransform("illegitimate","ALJT","ALKT");
	}

	@Test
	public void testTransform19() {
		// "immediately" - "AMTT"
		testTransform("immediately","AMTT","AMTT");
	}

	@Test
	public void tes20() {
		// "happily" - "HPL"
		testTransform("happily","HPL","HPL");
	}

	@Test
	public void testTransform21() {
		// "judgment" - "JTKM" and "ATKM"
		testTransform("judgment","JTKM","ATKM");
	}

	@Test
	public void testTransform22() {
		// "knowing" - "NNK"
		testTransform("knowing","NNK","NNK");
	}

	@Test
	public void testTransform23() {
		// "kipper" - "KPR"
		testTransform("kipper","KPR","KPR");
	}

	@Test
	public void testTransform24() {
		// "john" - "JN" and "AN"
		testTransform("john","JN","AN");
	}

	@Test
	public void testTransform25() {
		// "lesion" - "LSN" and "LXN"
		testTransform("lesion","LSN","LXN");
	}

	@Test
	public void testTransform26() {
		// "Xavier" - "SF" and "SFR"
		testTransform("Xavier","SF","SFR");
	}

	@Test
	public void testTransform27() {
		// "dumb" - "TM"
		testTransform("dumb","TM","TM");
	}

	@Test
	public void testTransform28() {
		// "caesar" - "SSR"
		testTransform("caesar","SSR","SSR");
	}

	@Test
	public void testTransform29() {
		// "chianti" - "KNT"
		testTransform("chianti","KNT","KNT");
	}

	@Test
	public void testTransform30() {
		// "michael" - "MKL" and "MXL"
		testTransform("michael","MKL","MXL");
	}

	@Test
	public void testTransform31() {
		// "chemistry" - "KMST"
		testTransform("chemistry","KMST","KMST");
	}

	@Test
	public void testTransform32() {
		// "chorus" - "KRS"
		testTransform("chorus","KRS","KRS");
	}

	@Test
	public void testTransform33() {
		// "architect - "ARKT"
		testTransform("architect","ARKT","ARKT");
	}

	@Test
	public void testTransform34() {
		// "arch" - "ARX" and "ARK"
		testTransform("arch","ARX","ARK");
	}

	@Test
	public void testTransform35() {
		// "orchestra" - "ARKS"
		testTransform("orchestra","ARKS","ARKS");
	}

	@Test
	public void testTransform36() {
		// "orchid" -  "ARKT"
		testTransform("orchid","ARKT","ARKT");
	}

	@Test
	public void testTransform37() {
		// "wachtler" - "AKTL" and "FKTL"
		testTransform("wachtler","AKTL","FKTL");
	}

	@Test
	public void testTransform38() {
		// "wechsler" - "AKSL" and "FKSL"
		testTransform("wechsler","AKSL","FKSL");
	}

	@Test
	public void testTransform39() {
		// "tichner" - "TXNR" and "TKNR"
		testTransform("tichner","TXNR","TKNR");
	}

	@Test
	public void testTransform40() {
		// "McHugh" - "MK"
		testTransform("McHugh","MK","MK");
	}

	@Test
	public void testTransform41() {
		// "czerny" - "SRN" and "XRN"
		testTransform("czerny","SRN","XRN");
	}

	@Test
	public void testTransform42() {
		// "focaccia" - "FKX"
		testTransform("focaccia","FKX","FKX");
	}

	@Test
	public void testTransform43() {
		// "bellocchio" - "PLX"
		testTransform("bellocchio","PLX","PLX");
	}

	@Test
	public void testTransform44() {
		// "bacchus" - "PKS"
		testTransform("bacchus","PKS","PKS");
	}

	@Test
	public void testTransform45() {
		// "accident" - "AKST"
		testTransform("accident","AKST","AKST");
	}

	@Test
	public void testTransform46() {
		// "accede" - "AKST"
		testTransform("accede","AKST","AKST");
	}

	@Test
	public void testTransform47() {
		// "succeed" - "SKST"
		testTransform("succeed","SKST","SKST");
	}

	@Test
	public void testTransform48() {
		// "bacci" - "PX"
		testTransform("bacci","PX","PX");
	}

	@Test
	public void testTransform49() {
		// "bertucci" - "PRTX"
		testTransform("bertucci","PRTX","PRTX");
	}

	@Test
	public void testTransform50() {
		// "mac caffrey" - "MKFR"
		testTransform("mac caffrey","MKFR","MKFR");
	}

	@Test
	public void testTransform51() {
		// "mac gregor" - "MKRK"
		testTransform("mac gregor","MKRK","MKRK");
	}

	@Test
	public void testTransform52() {
		// "edge" - "AJ"
		testTransform("edge","AJ","AJ");
	}

	@Test
	public void testTransform53() {
		// "edgar" - "ATKR"
		testTransform("edgar","ATKR","ATKR");
	}

	@Test
	public void testTransform54() {
		// "ghislane" - "JLN"
		testTransform("ghislane","JLN","JLN");
	}

	@Test
	public void testTransform55() {
		// ghiradelli - "JRTL"
		testTransform("ghiradelli","JRTL","JRTL");
	}

	@Test
	public void testTransform56() {
		// "hugh" - "H"
		testTransform("hugh","H","H");
	}

	@Test
	public void testTransform57() {
		// "bough" - "P"
		testTransform("bough","P","P");
	}

	@Test
	public void testTransform58() {
		// "broughton" - "PRTN"
		testTransform("broughton","PRTN","PRTN");
	}

	@Test
	public void testTransform59() {
		// "laugh" - "LF"
		testTransform("laugh","LF","LF");
	}

	@Test
	public void testTransform60() {
		// "McLaughlin" - "MKLF"
		testTransform("McLaughlin","MKLF","MKLF");
	}

	@Test
	public void testTransform61() {
		// "cough" - "KF"
		testTransform("cough","KF","KF");
	}

	@Test
	public void testTransform62() {
		// "gough" - "KF"
		testTransform("gough","KF","KF");
	}

	@Test
	public void testTransform63() {
		// "rough" - "RF"
		testTransform("rough","RF","RF");
	}

	@Test
	public void testTransform64() {
		// "tough" - "TF"
		testTransform("tough","TF","TF");
	}

	@Test
	public void testTransform65() {
		// "cagney" - "KKN"
		testTransform("cagney","KKN","KKN");
	}

	@Test
	public void testTransform66() {
		// "tagliaro" - "TKLR" and "TLR"
		testTransform("tagliaro","TKLR","TLR");
	}

	@Test
	public void testTransform67() {
		// "biaggi" - "PJ" and "PK"
		testTransform("biaggi","PJ","PK");
	}

	@Test
	public void testTransform68() {
		// "san jacinto" - "SNHS"
		testTransform("san jacinto","SNHS","SNHS");
	}

	@Test
	public void testTransform69() {
		// Yankelovich - "ANKL"
		testTransform("Yankelovich","ANKL","ANKL");
	}

	@Test
	public void testTransform70() {
		// Jankelowicz - "JNKL" and "ANKL"
		testTransform("Jankelowicz","JNKL","ANKL");
	}

	@Test
	public void testTransform71() {
		// "bajador" - "PJTR" and "PHTR"
		testTransform("bajador","PJTR","PHTR");
	}

	@Test
	public void testTransform72() {
		// "cabrillo" - "KPRL" and "KPR"
		testTransform("cabrillo","KPRL","KPR");
	}

	@Test
	public void testTransform73() {
		// "gallegos" - "KLKS" and "KKS"
		testTransform("gallegos","KLKS","KKS");
	}

	@Test
	public void testTransform74() {
		// "dumb" - "TM"
		testTransform("dumb","TM","TM");
	}

	@Test
	public void testTransform75() {
		// "thumb" - "0M" and "TM"
		testTransform("thumb","0M","TM");
	}

	@Test
	public void testTransform76() {
		// "campbell" - "KMPL"
		testTransform("campbell","KMPL","KMPL");
	}

	@Test
	public void testTransform77() {
		// "raspberry" - "RSPR"
		testTransform("raspberry","RSPR","RSPR");
	}

	@Test
	public void testTransform78() {
		// "hochmeier" - "HKMR"
		testTransform("hochmeier","HKMR","HKMR");
	}

	@Test
	public void testTransform79() {
		// "island" - "ALNT"
		testTransform("island","ALNT","ALNT");
	}

	@Test
	public void testTransform80() {
		// "isle" - "AL"
		testTransform("isle","AL","AL");
	}

	@Test
	public void testTransform81() {
		// "carlisle" - "KRLL"
		testTransform("carlisle","KRLL","KRLL");
	}

	@Test
	public void testTransform82() {
		// "carlysle" - "KRLL"
		testTransform("carlysle","KRLL","KRLL");
	}

	@Test
	public void testTransform83() {
		// "smith" - "SM0" and "XMT"
		testTransform("smith","SM0","XMT");
	}

	@Test
	public void testTransform84() {
		// "schmidt" - "XMT" and "SMT"
		testTransform("schmidt","XMT","SMT");
	}

	@Test
	public void testTransform85() {
		// "snider" - "SNTR" and "XNTR"
		testTransform("snider","SNTR","XNTR");
	}

	@Test
	public void testTransform86() {
		// "schneider" - "XNTR" and "SNTR"
		testTransform("schneider","XNTR","SNTR");
	}

	@Test
	public void testTransform87() {
		// "school" - "SKL"
		testTransform("school","SKL","SKL");
	}

	@Test
	public void testTransform88() {
		// "schooner" - "SKNR"
		testTransform("schooner","SKNR","SKNR");
	}

	@Test
	public void testTransform89() {
		// "schermerhorn" - "XRMR" and "SKRM"
		testTransform("schermerhorn","XRMR","SKRM");
	}

	@Test
	public void testTransform90() {
		// "schenker" - "XNKR" and "SKNK"
		testTransform("schenker","XNKR","SKNK");
	}

	@Test
	public void testTransform191() {
		// "resnais" - "RSN" and "RSNS"
		testTransform("resnais","RSN","RSNS");
	}

	@Test
	public void testTransform92() {
		// "artois" - "ART" and "ARTS"
		testTransform("artois","ART","ARTS");
	}

	@Test
	public void testTransform93() {
		// "thomas" - "TMS"
		testTransform("thomas","TMS","TMS");
	}

	@Test
	public void testTransform94() {
		// Wasserman - "ASRM" and "FSRM"
		testTransform("Wasserman","ASRM","FSRM");
	}

	@Test
	public void testTransform95() {
		// Vasserman - "FSRM"
		testTransform("Vasserman","FSRM","FSRM");
	}

	@Test
	public void testTransform96() {
		// Uomo - "AM"
		testTransform("Uomo","AM","AM");
	}

	@Test
	public void testTransform97() {
		// Womo - "AM" and "FM"
		testTransform("Womo","AM","FM");
	}

	@Test
	public void testTransform98() {
		// Arnow -  "ARN" and "ARNF"
		testTransform("Arnow","ARN","ARNF");
	}

	@Test
	public void testTransform99() {
		// Arnoff - "ARNF"
		testTransform("Arnoff","ARNF","ARNF");
	}

	@Test
	public void testTransform100() {
		// "filipowicz" - "FLPT" and "FLPF"
		testTransform("filipowicz","FLPT","FLPF");
	}

	@Test
	public void testTransform101() {
		// breaux - "PR"
		testTransform("breaux","PR","PR");
	}

	@Test
	public void testTransform102() {
		// "zhao" - "J"
		testTransform("zhao","J","J");
	}

	@Test
	public void testTransform103() {
		// "thames" - "TMS"
		testTransform("thames","TMS","TMS");
	}

	@Test
	public void testTransform104() {
		// "jose" - "HS" and "JS"
		testTransform("jose","HS","HS");
	}

	@Test
	public void testTransform105() {
		// "rogier" - "RJ" and "RJR"
		testTransform("rogier","RJ","RJR");
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Metaphone2").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Nelson","Neilsen"));
		}
	}

	@Test
	public void testBuildAndCompareLaxS() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("METAPHONE2(ALTERNATE)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Nelson","Neilsen"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Metaphone2(ABC)").build();
		if (func != null) {
		}
	}
}
