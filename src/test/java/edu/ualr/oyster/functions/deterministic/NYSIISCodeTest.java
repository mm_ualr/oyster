package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class NYSIISCodeTest {
	
	@Test
	public void testConstructor() {
		NYSIISCode func = new NYSIISCode();
		assertEquals(FunctionType.NYSIIS, func.getType());
		assertEquals(FunctionType.NYSIIS.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		NYSIISCode func = new NYSIISCode();
		assertEquals("FALAPS",func.transform("Philip Schmitt"));
		assertEquals("FALAPS",func.transform("Phillip Smith"));
		assertEquals("SNADTN",func.transform("SCHMIDT.KNOPH"));
		assertEquals("ABC",func.transform("a1B2C3E4"));
	}

	@Test
	public void testTokenize() {
		NYSIISCode func = new NYSIISCode();
		assertEquals("FALAPS",func.tokenize("Philip Schmitt")[0]);
	}

	@Test
	public void testTransformLax() {
		NYSIISCode func = new NYSIISCode();
		func.configure("LAX");
		assertEquals("FALAPSNAT",func.transform("Philip Schmitt"));
		assertEquals("FALAPSNAT",func.transform("Phillip Smith"));
	}

	@Test
	public void testCompare() {
		NYSIISCode func = new NYSIISCode();
		assertTrue(func.compare("Phillip Smith","Philip Schmitt"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("NYSIIS").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Phillip Smith","Philip Schmitt"));
		}
	}

	@Test
	public void testBuildAndCompareLaxS() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("NYSIIS(LAX)").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("Phillip Smith","Philip Schmitt"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("NYSIIS(ABC)").build();
		if (func != null) {
		}
	}
}
