package edu.ualr.oyster.functions.deterministic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import edu.ualr.oyster.functions.Compare;
import edu.ualr.oyster.functions.OysterFunction;
import edu.ualr.oyster.functions.OysterFunction.FunctionType;

public class FingerprintTest {

	@Test
	public void testConstructor() {
		Fingerprint func = new Fingerprint();
		assertEquals(FunctionType.FINGERPRINT, func.getType());
		assertEquals(FunctionType.FINGERPRINT.getFunctionName(),func.getName());
	}

	@Test
	public void testTransform() {
		Fingerprint func = new Fingerprint();
		assertEquals("a b c d e f ",func.transform("a$bcdef"));
		assertEquals("a b c d e f h i l o r t v ",func.transform("delta/Afla-CHARLIE.BrAvo"));
	}

	@Test
	public void testTokenize() {
		Fingerprint func = new Fingerprint();
		assertEquals("a b c d e f ",func.tokenize("a$bcdef")[0]);
	}

	@Test
	public void testCompare() {
		Fingerprint func = new Fingerprint();
		assertTrue(func.compare("abc.def","abcde,f"));
		assertTrue(func.compare("delta/Afla-CHARLIE.BrAvo","CHARLIE.BrAvo-delta/Afla"));
	}

	@Test
	public void testBuildAndCompare() {
		Compare func = null;
		try {
			func = new OysterFunction.Builder<Compare>("Fingerprint").build();
			//System.out.println(func.toString());
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException| IllegalArgumentException e) {
			e.printStackTrace();
		}
		if (func != null) {
			assertTrue(func.compare("abc.def","abcde,f"));
		}
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void testBuildAndCompareDefault() throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException {
		Compare func = new OysterFunction.Builder<Compare>("Fingerprint(ABC)").build();
		if (func != null) {
		}
	}

}
