package edu.ualr.oyster.reference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

public class TokenWeightTableReferenceTest {

	@Test(expected = IllegalStateException.class)
	public void testGetMissingWeightTable() {
		TokenWeightTableReference reference = TokenWeightTableReference.getInstance("data/notfound.dat");
		assertNull(reference);
	}

	@Test(expected = IllegalStateException.class)
	public void testGetBadWeightTable() {
		TokenWeightTableReference reference = TokenWeightTableReference.getInstance("data/badWeightTable.dat");
		assertNull(reference);
	}

	@Test
	public void testGetWeightTableDeafult() {
		TokenWeightTableReference reference = TokenWeightTableReference.getInstance();
		assertNotNull(reference);
		Map<String, Float> table = reference.getTokenWeightTable();
		assertEquals(13596, table.size());
	}

	@Test
	public void testisStopWord() {
		TokenWeightTableReference reference = TokenWeightTableReference.getInstance("data/agreeWeightTable.dat");
		assertNotNull(reference);
		Map<String, Float> table = reference.getTokenWeightTable();
		assertEquals(6, table.size());
		assertTrue(reference.isStopWord("anDerson", 0.7f));
		assertFalse(reference.isStopWord("FARMSVILLE", 0.7f));
	}

	@Test
	public void testGetTokenWeight() {
		TokenWeightTableReference reference = TokenWeightTableReference.getInstance("data/agreeWeightTable.dat");
		assertNotNull(reference);
		Map<String, Float> table = reference.getTokenWeightTable();
		assertEquals(6, table.size());
		assertEquals(1.0, reference.getTokenWeight("MISSINGWORD"), 0.1);
		assertEquals(0.5, reference.getTokenWeight("POINT5"), 0.1);
	}
}
