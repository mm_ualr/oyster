package edu.ualr.oyster.reference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

public class WeightTableReferenceTest {

	@Test(expected = IllegalStateException.class)
	public void testGetMissingWeightTable() {
		WeightTableReference reference = WeightTableReference.getInstance("data/notfound.dat");
		assertNull(reference);
	}

	@Test(expected = IllegalStateException.class)
	public void testGetBadWeightTable() {
		WeightTableReference reference = WeightTableReference.getInstance("data/badWeightTable.dat");
		assertNull(reference);
	}

	@Test
	public void testGetWeightTable() {
		WeightTableReference reference = WeightTableReference.getInstance("data/tokenWeightTable.dat");
		assertNotNull(reference);
		Map<String, WeightPair> table = reference.getWeightTable();
		assertEquals(13596, table.size());
		assertFalse(reference.usesDisagreement());
	}

	@Test
	public void testAddWeightPair() {
		WeightTableReference reference = WeightTableReference.getInstance("data/agreeWeightTable.dat");
		assertNotNull(reference);
		Map<String, WeightPair> table = reference.getWeightTable();
		assertEquals(6, table.size());
		reference.addWeightPair("TESTWORD", new WeightPair(1.0));
		WeightPair pair = reference.getWeightPair("TESTWORD");
		assertNotNull(pair);
		assertEquals(1.0, pair.getAgreeWgt(), 0.1);
		assertEquals(0.0, pair.getDisagreeWgt(), 0.1);
		assertFalse(reference.usesDisagreement());
	}

	@Test
	public void testAddDisagreeWeightPair() {
		WeightTableReference reference = WeightTableReference.getInstance("data/disagreeWeightTable.dat");
		assertNotNull(reference);
		Map<String, WeightPair> table = reference.getWeightTable();
		assertEquals(3, table.size());
		reference.addWeightPair("TESTWORD", new WeightPair(1.0, 2.0));
		WeightPair pair = reference.getWeightPair("TESTWORD");
		assertNotNull(pair);
		assertEquals(1.0, pair.getAgreeWgt(), 0.1);
		assertEquals(2.0, pair.getDisagreeWgt(), 0.1);
		assertTrue(reference.usesDisagreement());
	}
}
