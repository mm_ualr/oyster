package edu.ualr.oyster.reference;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;

public class AliasReferenceTest {

	@Test
	public void test() {
		AliasReference reference1 = AliasReference.getInstance("data/alias.dat");
		assertNotNull(reference1);
		Map<String, ArrayList<String>> table1 = reference1.getAliasTable();
		System.out.println("Reference 1:" + reference1 + " size:"+table1.size());
		
		AliasReference reference2 = AliasReference.getInstance("data/alias.dat");
		assertNotNull(reference2);
		Map<String, ArrayList<String>> table2 = reference2.getAliasTable();
		System.out.println("Reference 2:" + reference2 + " size:"+table2.size());
		
		AliasReference reference3 = AliasReference.getInstance("data/alias-redux.dat");
		assertNotNull(reference3);
		Map<String, ArrayList<String>> table3 = reference3.getAliasTable();
		System.out.println("Reference 3:" + reference3 + " size:"+table3.size());

		assertThat(reference1, is(equalTo(reference2)));
		assertThat(reference1, not(equalTo(reference3)));
		assertThat(reference2, not(equalTo(reference3)));
	}

}
