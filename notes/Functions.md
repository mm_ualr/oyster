# OYSTER v3.x Function List

This list of functions is extracted from the edu.ualr.oyster.association.matching.OysterComparatorXXXXX classes. 
These have been or will be migrated to edu.ualr.ouster.functions using the new OysterFunction base class and the defined functional interfaces: Compare, Score, Tokenize, Transform

### OysterCompare (base class)
- *Missing : either attribute is missing
- Exact : exact match, case sensitive
- *Exact_Ignore_Case : exact match, case insensitive
- *ExactOrBothMissing : exact match, or both parameters null or blank
- *ExactOrEitherMissing : exact match, or either parameter is null or blank

### OysterCompareDefault (only comparator currently in use)

- ALIAS
	Transform only
- CAVERPHONE
- CAVERPHONE2
- COSINE
- DMSOUNDEX
- EXACTORNICKNAME
- IBMALPHACODE (doc says IBMALPHA)
- INITIAL
- JACCARD(Threshold(float))
- JAROWINKLER
- LED(Threshold(float))
- LISTOVERLAP(???)
- LISTOVERLAPPV(???)
- MATCHRATING
- MATRIXCOMPARATOR(threshold, excludelist)
	Compare only
- MATRIXTOKENIZER(minLength, exludelist)
	Tokenize only
- MAXQGRAM
- METAPHONE
- METAPHONE2
- MISSING(EITHER|BOTH)
- NICKNAME
	Transform only
- NYSIIS
- PSUBSTR(Length(int)) - compare substrings with minimum length
- QTR(Threshold(float)) -
- SCAN(direction, charType, length(int), upperCase, order)
- SMITHWATERMAN
- SORENSEN(Threshold(float))
- SOUNDEX
- SUBSTRLEFT(Length(int)) - left n characters of string
- SUBSTRRIGHT(Length(int)) - right n characters of string
- SUBSTRMID(int Start, Length(int)) - mid characters start for length
- TANIMOTO(Threshold(float))
- TRANSPOSE
- TVERSKY(Threshold(float), Alpha(float), Beta(float)) - 

###Utilities that have been migrated but are still referenced but cannot yet be deleted
- CharacterSubstringMatchesX
- MatrixComparatorX
  SmithWatermanX

###Utilities with complications or that need further work
- ListOverlap - threshold, and delimiter parameters to compare method, configure with parameters
- ListOverlapPV - threshold, list delimiter, pair delimiter, stop-words parameters to compare method, configure with parameters
- ListTokenizer
- ListTokenizerPV
- MatrixComparator2 - threshold, excluded token list parameters to compare method, configure with parameters
- MatrixComparatorWeighted - threshold and OysterTokenWeightTable parameters to compare method, configure with parameters
- MatrixTOkenizerWeightes
- Metaphone3
- NEEDLEMANWUNSCH(float match, mismatch, gap, threshold)
- NGramFingerprint
- OysterADENickNameTable
- OysterKeyboardEditDistance
- QGramTetrahedralRationCyclic
- ChinesePinYin
- SOUNDEXORNICKNAME